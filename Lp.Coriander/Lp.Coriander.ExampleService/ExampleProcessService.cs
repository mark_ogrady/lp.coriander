﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Lp.Coriander.ExampleService
{
    public partial class ExampleProcessService : ServiceBase
    {

        static readonly ExampleProcessService instance = new ExampleProcessService();

        public static ExampleProcessService Instance
        {
            get { return instance; }
        }

        public ExampleProcessService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
        }

        protected override void OnStop()
        {
        }
    }
}
