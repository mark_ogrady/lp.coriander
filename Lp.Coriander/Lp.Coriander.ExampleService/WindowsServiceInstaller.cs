﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace Lp.Coriander.ExampleService
{
    [RunInstaller(true)]
    public partial class WindowsServiceInstaller : System.Configuration.Install.Installer
    {
        public WindowsServiceInstaller()
        {
            ServiceProcessInstaller process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.LocalSystem;

            ServiceInstaller serviceAdmin = new ServiceInstaller();
            serviceAdmin.StartType = ServiceStartMode.Automatic;

            var serviceName = ConfigurationManager.AppSettings["ServiceName"];
            serviceAdmin.ServiceName = serviceName;
            serviceAdmin.DisplayName = serviceName;
            serviceAdmin.Description = serviceName;
            Installers.Add(process);
            Installers.Add(serviceAdmin);
        }
    }
}
