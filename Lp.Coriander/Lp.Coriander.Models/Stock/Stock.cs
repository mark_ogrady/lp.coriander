﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lp.Coriander.TransferModels.Stock
{
    public class Stock
    {
        public int StockId { get; set; }
        public string StockItemCode { get; set; }
        public string Description { get; set; }

    }
}
