﻿using System.Collections.Generic;
using Lp.Coriander.Models.Base;

namespace Lp.Coriander.TransferModels.SOP
{
    public class SopOrder :IBaseTransferModel
    {
        public SopOrder()
        {
            CustomValues = new Dictionary<string, string>();
            SopOrderLines = new List<SOPOrderLine>();
        }

        public List<SOPOrderLine> SopOrderLines { get; set; }
        public Dictionary<string, string> CustomValues { get; set; }
        
    }

    public class SOPOrderLine : IBaseTransferModel
    {
        public SOPOrderLine()
        {
            CustomValues = new Dictionary<string, string>();
        }


        public Dictionary<string, string> CustomValues { get; set; }
    }
}
