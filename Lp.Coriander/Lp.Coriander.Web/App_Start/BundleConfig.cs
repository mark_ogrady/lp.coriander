﻿using System.Web;
using System.Web.Optimization;

namespace Lp.Coriander.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

         

            #region App

            bundles.Add(new StyleBundle("~/Styles/appcss").Include(
                "~/Content/Admin/plugins/dropzone/css/dropzone.css",
                "~/Content/Admin/plugins/gritter/css/jquery.gritter.css",
                "~/Content/Admin/plugins/bootstrap-datepicker/css/datepicker.css",
                "~/Content/Admin/plugins/jquery-ricksaw-chart/css/rickshaw.css",
                "~/Content/Admin/plugins/jquery-morris-chart/css/morris.css",
                "~/Content/Admin/plugins/jquery-slider/css/jquery.sidr.light.css",
                "~/Content/Admin/plugins/bootstrap-select2/select2.css",
                "~/Content/Admin/plugins/jquery-jvectormap/css/jquery-jvectormap-1.2.2.css",
                "~/Content/Admin/plugins/boostrap-checkbox/css/bootstrap-checkbox.css",
                "~/Content/Admin/plugins/gritter/css/jquery.gritter.css",
                "~/Content/Admin/plugins/ios-switch/ios7-switch.css",
                "~/Content/Admin/plugins/imagepicker/css/image-picker.css",
                "~/Content/Admin/plugins/jquery-nestable/jquery.nestable.css",
                "~/Content/Admin/plugins/jquery-datatable/css/jquery.dataTables.css",
                "~/Content/Admin/plugins/datatables-responsive/css/datatables.responsive.css",
                "~/Content/Admin/plugins/jquery-notifications/css/messenger.css",
                "~/Content/Admin/plugins/jquery-notifications/css/messenger-theme-flat.css",
                "~/Content/Admin/plugins/jquery-notifications/css/location-sel.css",
                "~/Content/Admin/plugins/boostrapv3/css/bootstrap.min.css",
                "~/Content/Admin/plugins/boostrapv3/css/bootstrap-theme.min.css",
                "~/Content/Admin/plugins/font-awesome/css/font-awesome.css",
                "~/Content/Admin/css/animate.min.css",
                "~/Content/Admin/css/style.css",
                "~/Content/Admin/css/responsive.css",
                "~/Content/Admin/css/custom-icon-set.css"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                 "~/Content/Admin/plugins/jquery-1.8.3.min.js",
    "~/Content/Admin/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js",
    "~/Content/Admin/plugins/boostrapv3/js/bootstrap.min.js",
    "~/Content/Admin/plugins/breakpoints.js",
    "~/Content/Admin/plugins/jquery-unveil/jquery.unveil.min.js",
    "~/Content/Admin/plugins/jquery.touchpunch/jquery.ui.touch-punch.min.js",
    "~/Content/Admin/plugins/pace/pace.min.js",
    "~/Content/Admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
    "~/Content/Admin/plugins/jquery-numberAnimate/jquery.animateNumbers.js",
    "~/Content/Admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js",
    "~/Content/Admin/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js",
    "~/Content/Admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
    "~/Content/Admin/plugins/jquery-block-ui/jqueryblockui.js",
    "~/Content/Admin/plugins/bootstrap-select2/select2.min.js",
    "~/Content/Admin/plugins/jquery-ricksaw-chart/js/raphael-min.js",
    "~/Content/Admin/plugins/jquery-ricksaw-chart/js/d3.v2.js",
    "~/Content/Admin/plugins/jquery-ricksaw-chart/js/rickshaw.min.js",
    "~/Content/Admin/plugins/jquery-morris-chart/js/morris.min.js",
    "~/Content/Admin/plugins/jquery-easy-pie-chart/js/jquery.easypiechart.min.js",
    "~/Content/Admin/plugins/jquery-slider/jquery.sidr.min.js",
    "~/Content/Admin/plugins/jquery-jvectormap/js/jquery-jvectormap-1.2.2.min.js",
    "~/Content/Admin/plugins/jquery-jvectormap/js/jquery.vmap.europe.js",
    "~/Content/Admin/plugins/jquery-sparkline/jquery-sparkline.js",
    "~/Content/Admin/plugins/jquery-flot/jquery.flot.min.js",
    "~/Content/Admin/plugins/jquery-flot/jquery.flot.animator.min.js",
    "~/Content/Admin/plugins/skycons/skycons.js",
    "~/Content/Admin/plugins/jquery-nestable/jquery.nestable.js",
    "~/Content/Admin/plugins/jquery-datatable/js/jquery.dataTables.min.js",
    "~/Content/Admin/plugins/jquery-datatable/extra/js/TableTools.min.js",
    "~/Content/Admin/plugins/datatables-responsive/js/datatables.responsive.js",
    "~/Content/Admin/plugins/datatables-responsive/js/lodash.min.js",
    "~/Content/Admin/plugins/jquery-notifications/js/messenger.min.js",
    "~/Content/Admin/plugins/jquery-notifications/js/messenger-theme-future.js",
    "~/Content/Admin/plugins/jquery-notifications/js/demo/location-sel.js",
    "~/Content/Admin/plugins/jquery-notifications/js/demo/theme-sel.js",
    "~/Content/Admin/plugins/jquery-notifications/js/demo/demo.js",
    "~/Content/Admin/js/notifications.js",
    "~/Content/Admin/plugins/jquery-datatable/extra/js/ZeroClipboard.js",
    "~/Content/Admin/js/datatables.js"


                ));

            #endregion
            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
           
        }
    }
}
