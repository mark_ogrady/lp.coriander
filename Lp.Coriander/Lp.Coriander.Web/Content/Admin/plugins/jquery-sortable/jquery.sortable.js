




<!DOCTYPE html>
<html class="   ">
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object# article: http://ogp.me/ns/article# profile: http://ogp.me/ns/profile#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    
    <title>html5sortable/jquery.sortable.js at master · farhadi/html5sortable · GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub" />
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub" />
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-144.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144.png" />
    <meta property="fb:app_id" content="1401488693436528"/>

      <meta content="@github" name="twitter:site" /><meta content="summary" name="twitter:card" /><meta content="farhadi/html5sortable" name="twitter:title" /><meta content="html5sortable - Lightweight jQuery plugin to create sortable lists and grids using native HTML5 drag and drop API." name="twitter:description" /><meta content="https://avatars2.githubusercontent.com/u/102497?s=400" name="twitter:image:src" />
<meta content="GitHub" property="og:site_name" /><meta content="object" property="og:type" /><meta content="https://avatars2.githubusercontent.com/u/102497?s=400" property="og:image" /><meta content="farhadi/html5sortable" property="og:title" /><meta content="https://github.com/farhadi/html5sortable" property="og:url" /><meta content="html5sortable - Lightweight jQuery plugin to create sortable lists and grids using native HTML5 drag and drop API." property="og:description" />

    <link rel="assets" href="https://github.global.ssl.fastly.net/">
    <link rel="conduit-xhr" href="https://ghconduit.com:25035/">
    <link rel="xhr-socket" href="/_sockets" />

    <meta name="msapplication-TileImage" content="/windows-tile.png" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="selected-link" value="repo_source" data-pjax-transient />
    <meta content="collector.githubapp.com" name="octolytics-host" /><meta content="collector-cdn.github.com" name="octolytics-script-host" /><meta content="github" name="octolytics-app-id" /><meta content="52456262:2880:15FEDFC:535D1EE2" name="octolytics-dimension-request_id" />
    

    
    
    <link rel="icon" type="image/x-icon" href="https://github.global.ssl.fastly.net/favicon.ico" />

    <meta content="authenticity_token" name="csrf-param" />
<meta content="m7Q7SeCEk+j5IGl45f+aooY+S9lOmo9ImghaDpHh1Y82FPyer28BFD7BVMrR2BrUFlZZFc49Xq+0ndSETBlrew==" name="csrf-token" />

    <link href="https://github.global.ssl.fastly.net/assets/github-cc557869fb9cdb388d66a57c972147dfcf6cc34f.css" media="all" rel="stylesheet" type="text/css" />
    <link href="https://github.global.ssl.fastly.net/assets/github2-1202793039fee08f007a3b9e0dd1192c81e4c31d.css" media="all" rel="stylesheet" type="text/css" />
    


        <script crossorigin="anonymous" src="https://github.global.ssl.fastly.net/assets/frameworks-83bb02a73dd69926304fd1a101dbd75aa669e8dd.js" type="text/javascript"></script>
        <script async="async" crossorigin="anonymous" src="https://github.global.ssl.fastly.net/assets/github-1d850079447c8a04da81d39c2573cedf09aae011.js" type="text/javascript"></script>
        
        
      <meta http-equiv="x-pjax-version" content="ac84ed3e1ff52b563c520bd0abcbca43">

        <link data-pjax-transient rel='permalink' href='/farhadi/html5sortable/blob/b90b6baac95cdc0df1ec1682b7181ff94b1f7820/jquery.sortable.js'>

  <meta name="description" content="html5sortable - Lightweight jQuery plugin to create sortable lists and grids using native HTML5 drag and drop API." />

  <meta content="102497" name="octolytics-dimension-user_id" /><meta content="farhadi" name="octolytics-dimension-user_login" /><meta content="3287270" name="octolytics-dimension-repository_id" /><meta content="farhadi/html5sortable" name="octolytics-dimension-repository_nwo" /><meta content="true" name="octolytics-dimension-repository_public" /><meta content="false" name="octolytics-dimension-repository_is_fork" /><meta content="3287270" name="octolytics-dimension-repository_network_root_id" /><meta content="farhadi/html5sortable" name="octolytics-dimension-repository_network_root_nwo" />
  <link href="https://github.com/farhadi/html5sortable/commits/master.atom" rel="alternate" title="Recent Commits to html5sortable:master" type="application/atom+xml" />

  </head>


  <body class="logged_out  env-production windows vis-public page-blob">
    <a href="#start-of-content" tabindex="1" class="accessibility-aid js-skip-to-content">Skip to content</a>
    <div class="wrapper">
      
      
      
      


      
      <div class="header header-logged-out">
  <div class="container clearfix">

    <a class="header-logo-wordmark" href="https://github.com/">
      <span class="mega-octicon octicon-logo-github"></span>
    </a>

    <div class="header-actions">
        <a class="button primary" href="/join">Sign up</a>
      <a class="button signin" href="/login?return_to=%2Ffarhadi%2Fhtml5sortable%2Fblob%2Fmaster%2Fjquery.sortable.js">Sign in</a>
    </div>

    <div class="command-bar js-command-bar  in-repository">

      <ul class="top-nav">
          <li class="explore"><a href="/explore">Explore</a></li>
        <li class="features"><a href="/features">Features</a></li>
          <li class="enterprise"><a href="https://enterprise.github.com/">Enterprise</a></li>
          <li class="blog"><a href="/blog">Blog</a></li>
      </ul>
        <form accept-charset="UTF-8" action="/search" class="command-bar-form" id="top_search_form" method="get">

<div class="commandbar">
  <span class="message"></span>
  <input type="text" data-hotkey="s, /" name="q" id="js-command-bar-field" placeholder="Search or type a command" tabindex="1" autocapitalize="off"
    
    
      data-repo="farhadi/html5sortable"
      data-branch="master"
      data-sha="efba0ace0824cc4ef0895d2b6f4d404c27ccc0f9"
  >
  <div class="display hidden"></div>
</div>

    <input type="hidden" name="nwo" value="farhadi/html5sortable" />

    <div class="select-menu js-menu-container js-select-menu search-context-select-menu">
      <span class="minibutton select-menu-button js-menu-target" role="button" aria-haspopup="true">
        <span class="js-select-button">This repository</span>
      </span>

      <div class="select-menu-modal-holder js-menu-content js-navigation-container" aria-hidden="true">
        <div class="select-menu-modal">

          <div class="select-menu-item js-navigation-item js-this-repository-navigation-item selected">
            <span class="select-menu-item-icon octicon octicon-check"></span>
            <input type="radio" class="js-search-this-repository" name="search_target" value="repository" checked="checked" />
            <div class="select-menu-item-text js-select-button-text">This repository</div>
          </div> <!-- /.select-menu-item -->

          <div class="select-menu-item js-navigation-item js-all-repositories-navigation-item">
            <span class="select-menu-item-icon octicon octicon-check"></span>
            <input type="radio" name="search_target" value="global" />
            <div class="select-menu-item-text js-select-button-text">All repositories</div>
          </div> <!-- /.select-menu-item -->

        </div>
      </div>
    </div>

  <span class="help tooltipped tooltipped-s" aria-label="Show command bar help">
    <span class="octicon octicon-question"></span>
  </span>


  <input type="hidden" name="ref" value="cmdform">

</form>
    </div>

  </div>
</div>



      <div id="start-of-content" class="accessibility-aid"></div>
          <div class="site" itemscope itemtype="http://schema.org/WebPage">
    <div id="js-flash-container">
      
    </div>
    <div class="pagehead repohead instapaper_ignore readability-menu">
      <div class="container">
        

<ul class="pagehead-actions">


  <li>
    <a href="/login?return_to=%2Ffarhadi%2Fhtml5sortable"
    class="minibutton with-count js-toggler-target star-button tooltipped tooltipped-n"
    aria-label="You must be signed in to star a repository" rel="nofollow">
    <span class="octicon octicon-star"></span>Star
  </a>

    <a class="social-count js-social-count" href="/farhadi/html5sortable/stargazers">
      984
    </a>

  </li>

    <li>
      <a href="/login?return_to=%2Ffarhadi%2Fhtml5sortable"
        class="minibutton with-count js-toggler-target fork-button tooltipped tooltipped-n"
        aria-label="You must be signed in to fork a repository" rel="nofollow">
        <span class="octicon octicon-git-branch"></span>Fork
      </a>
      <a href="/farhadi/html5sortable/network" class="social-count">
        266
      </a>
    </li>
</ul>

        <h1 itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="entry-title public">
          <span class="repo-label"><span>public</span></span>
          <span class="mega-octicon octicon-repo"></span>
          <span class="author"><a href="/farhadi" class="url fn" itemprop="url" rel="author"><span itemprop="title">farhadi</span></a></span><!--
       --><span class="path-divider">/</span><!--
       --><strong><a href="/farhadi/html5sortable" class="js-current-repository js-repo-home-link">html5sortable</a></strong>

          <span class="page-context-loader">
            <img alt="Octocat-spinner-32" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
          </span>

        </h1>
      </div><!-- /.container -->
    </div><!-- /.repohead -->

    <div class="container">
      <div class="repository-with-sidebar repo-container new-discussion-timeline js-new-discussion-timeline  ">
        <div class="repository-sidebar clearfix">
            

<div class="sunken-menu vertical-right repo-nav js-repo-nav js-repository-container-pjax js-octicon-loaders">
  <div class="sunken-menu-contents">
    <ul class="sunken-menu-group">
      <li class="tooltipped tooltipped-w" aria-label="Code">
        <a href="/farhadi/html5sortable" aria-label="Code" class="selected js-selected-navigation-item sunken-menu-item" data-hotkey="g c" data-pjax="true" data-selected-links="repo_source repo_downloads repo_commits repo_tags repo_branches /farhadi/html5sortable">
          <span class="octicon octicon-code"></span> <span class="full-word">Code</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

        <li class="tooltipped tooltipped-w" aria-label="Issues">
          <a href="/farhadi/html5sortable/issues" aria-label="Issues" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-hotkey="g i" data-selected-links="repo_issues /farhadi/html5sortable/issues">
            <span class="octicon octicon-issue-opened"></span> <span class="full-word">Issues</span>
            <span class='counter'>62</span>
            <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>        </li>

      <li class="tooltipped tooltipped-w" aria-label="Pull Requests">
        <a href="/farhadi/html5sortable/pulls" aria-label="Pull Requests" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-hotkey="g p" data-selected-links="repo_pulls /farhadi/html5sortable/pulls">
            <span class="octicon octicon-git-pull-request"></span> <span class="full-word">Pull Requests</span>
            <span class='counter'>23</span>
            <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>


    </ul>
    <div class="sunken-menu-separator"></div>
    <ul class="sunken-menu-group">

      <li class="tooltipped tooltipped-w" aria-label="Pulse">
        <a href="/farhadi/html5sortable/pulse" aria-label="Pulse" class="js-selected-navigation-item sunken-menu-item" data-pjax="true" data-selected-links="pulse /farhadi/html5sortable/pulse">
          <span class="octicon octicon-pulse"></span> <span class="full-word">Pulse</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

      <li class="tooltipped tooltipped-w" aria-label="Graphs">
        <a href="/farhadi/html5sortable/graphs" aria-label="Graphs" class="js-selected-navigation-item sunken-menu-item" data-pjax="true" data-selected-links="repo_graphs repo_contributors /farhadi/html5sortable/graphs">
          <span class="octicon octicon-graph"></span> <span class="full-word">Graphs</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

      <li class="tooltipped tooltipped-w" aria-label="Network">
        <a href="/farhadi/html5sortable/network" aria-label="Network" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-selected-links="repo_network /farhadi/html5sortable/network">
          <span class="octicon octicon-git-branch"></span> <span class="full-word">Network</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>
    </ul>


  </div>
</div>

              <div class="only-with-full-nav">
                

  

<div class="clone-url open"
  data-protocol-type="http"
  data-url="/users/set_protocol?protocol_selector=http&amp;protocol_type=clone">
  <h3><strong>HTTPS</strong> clone URL</h3>
  <div class="clone-url-box">
    <input type="text" class="clone js-url-field"
           value="https://github.com/farhadi/html5sortable.git" readonly="readonly">

    <span aria-label="copy to clipboard" class="js-zeroclipboard url-box-clippy minibutton zeroclipboard-button" data-clipboard-text="https://github.com/farhadi/html5sortable.git" data-copied-hint="copied!"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>

  

<div class="clone-url "
  data-protocol-type="subversion"
  data-url="/users/set_protocol?protocol_selector=subversion&amp;protocol_type=clone">
  <h3><strong>Subversion</strong> checkout URL</h3>
  <div class="clone-url-box">
    <input type="text" class="clone js-url-field"
           value="https://github.com/farhadi/html5sortable" readonly="readonly">

    <span aria-label="copy to clipboard" class="js-zeroclipboard url-box-clippy minibutton zeroclipboard-button" data-clipboard-text="https://github.com/farhadi/html5sortable" data-copied-hint="copied!"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>


<p class="clone-options">You can clone with
      <a href="#" class="js-clone-selector" data-protocol="http">HTTPS</a>
      or <a href="#" class="js-clone-selector" data-protocol="subversion">Subversion</a>.
  <span class="help tooltipped tooltipped-n" aria-label="Get help on which URL is right for you.">
    <a href="https://help.github.com/articles/which-remote-url-should-i-use">
    <span class="octicon octicon-question"></span>
    </a>
  </span>
</p>


  <a href="http://windows.github.com" class="minibutton sidebar-button" title="Save farhadi/html5sortable to your computer and use it in GitHub Desktop." aria-label="Save farhadi/html5sortable to your computer and use it in GitHub Desktop.">
    <span class="octicon octicon-device-desktop"></span>
    Clone in Desktop
  </a>

                <a href="/farhadi/html5sortable/archive/master.zip"
                   class="minibutton sidebar-button"
                   aria-label="Download farhadi/html5sortable as a zip file"
                   title="Download farhadi/html5sortable as a zip file"
                   rel="nofollow">
                  <span class="octicon octicon-cloud-download"></span>
                  Download ZIP
                </a>
              </div>
        </div><!-- /.repository-sidebar -->

        <div id="js-repo-pjax-container" class="repository-content context-loader-container" data-pjax-container>
          


<!-- blob contrib key: blob_contributors:v21:06ba17927675755358e6baaeb208910a -->

<p title="This is a placeholder element" class="js-history-link-replace hidden"></p>

<a href="/farhadi/html5sortable/find/master" data-pjax data-hotkey="t" class="js-show-file-finder" style="display:none">Show File Finder</a>

<div class="file-navigation">
  

<div class="select-menu js-menu-container js-select-menu" >
  <span class="minibutton select-menu-button js-menu-target" data-hotkey="w"
    data-master-branch="master"
    data-ref="master"
    role="button" aria-label="Switch branches or tags" tabindex="0" aria-haspopup="true">
    <span class="octicon octicon-git-branch"></span>
    <i>branch:</i>
    <span class="js-select-button">master</span>
  </span>

  <div class="select-menu-modal-holder js-menu-content js-navigation-container" data-pjax aria-hidden="true">

    <div class="select-menu-modal">
      <div class="select-menu-header">
        <span class="select-menu-title">Switch branches/tags</span>
        <span class="octicon octicon-remove-close js-menu-close"></span>
      </div> <!-- /.select-menu-header -->

      <div class="select-menu-filters">
        <div class="select-menu-text-filter">
          <input type="text" aria-label="Filter branches/tags" id="context-commitish-filter-field" class="js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
        </div>
        <div class="select-menu-tabs">
          <ul>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="branches" class="js-select-menu-tab">Branches</a>
            </li>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="tags" class="js-select-menu-tab">Tags</a>
            </li>
          </ul>
        </div><!-- /.select-menu-tabs -->
      </div><!-- /.select-menu-filters -->

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="branches">

        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/farhadi/html5sortable/blob/gh-pages/jquery.sortable.js"
                 data-name="gh-pages"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="gh-pages">gh-pages</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item selected">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/farhadi/html5sortable/blob/master/jquery.sortable.js"
                 data-name="master"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="master">master</a>
            </div> <!-- /.select-menu-item -->
        </div>

          <div class="select-menu-no-results">Nothing to show</div>
      </div> <!-- /.select-menu-list -->

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="tags">
        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


        </div>

        <div class="select-menu-no-results">Nothing to show</div>
      </div> <!-- /.select-menu-list -->

    </div> <!-- /.select-menu-modal -->
  </div> <!-- /.select-menu-modal-holder -->
</div> <!-- /.select-menu -->

  <div class="breadcrumb">
    <span class='repo-root js-repo-root'><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/farhadi/html5sortable" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">html5sortable</span></a></span></span><span class="separator"> / </span><strong class="final-path">jquery.sortable.js</strong> <span aria-label="copy to clipboard" class="js-zeroclipboard minibutton zeroclipboard-button" data-clipboard-text="jquery.sortable.js" data-copied-hint="copied!"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>


  <div class="commit file-history-tease">
    <img alt="Ali Farhadi" class="main-avatar js-avatar" data-user="102497" height="24" src="https://avatars1.githubusercontent.com/u/102497?s=140" width="24" />
    <span class="author"><a href="/farhadi" rel="author">farhadi</a></span>
    <time class="js-relative-date" data-title-format="YYYY-MM-DD HH:mm:ss" datetime="2012-07-29T12:51:34+04:30" title="2012-07-29 13:51:34">July 29, 2012</time>
    <div class="commit-title">
        <a href="/farhadi/html5sortable/commit/040fa49bcaef5a8334b0774367d23d05b2cb8629" class="message" data-pjax="true" title="In Opera `dragend` isn&#39;t triggred after `drop` event, so we trigger it m...

...anually. Closes #16.">In Opera `dragend` isn't triggred after `drop` event, so we trigger i…</a>
    </div>

    <div class="participation">
      <p class="quickstat"><a href="#blob_contributors_box" rel="facebox"><strong>2</strong>  contributors</a></p>
          <a class="avatar tooltipped tooltipped-s" aria-label="farhadi" href="/farhadi/html5sortable/commits/master/jquery.sortable.js?author=farhadi"><img alt="Ali Farhadi" class=" js-avatar" data-user="102497" height="20" src="https://avatars1.githubusercontent.com/u/102497?s=140" width="20" /></a>
    <a class="avatar tooltipped tooltipped-s" aria-label="OscarGodson" href="/farhadi/html5sortable/commits/master/jquery.sortable.js?author=OscarGodson"><img alt="Oscar Godson" class=" js-avatar" data-user="20615" height="20" src="https://avatars3.githubusercontent.com/u/20615?s=140" width="20" /></a>


    </div>
    <div id="blob_contributors_box" style="display:none">
      <h2 class="facebox-header">Users who have contributed to this file</h2>
      <ul class="facebox-user-list">
          <li class="facebox-user-list-item">
            <img alt="Ali Farhadi" class=" js-avatar" data-user="102497" height="24" src="https://avatars1.githubusercontent.com/u/102497?s=140" width="24" />
            <a href="/farhadi">farhadi</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="Oscar Godson" class=" js-avatar" data-user="20615" height="24" src="https://avatars3.githubusercontent.com/u/20615?s=140" width="24" />
            <a href="/OscarGodson">OscarGodson</a>
          </li>
      </ul>
    </div>
  </div>

<div class="file-box">
  <div class="file">
    <div class="meta clearfix">
      <div class="info file-name">
        <span class="icon"><b class="octicon octicon-file-text"></b></span>
        <span class="mode" title="File Mode">file</span>
        <span class="meta-divider"></span>
          <span>86 lines (85 sloc)</span>
          <span class="meta-divider"></span>
        <span>2.823 kb</span>
      </div>
      <div class="actions">
        <div class="button-group">
            <a class="minibutton tooltipped tooltipped-w"
               href="http://windows.github.com" aria-label="Open this file in GitHub for Windows">
                <span class="octicon octicon-device-desktop"></span> Open
            </a>
              <a class="minibutton disabled tooltipped tooltipped-w" href="#"
                 aria-label="You must be signed in to make or propose changes">Edit</a>
          <a href="/farhadi/html5sortable/raw/master/jquery.sortable.js" class="button minibutton " id="raw-url">Raw</a>
            <a href="/farhadi/html5sortable/blame/master/jquery.sortable.js" class="button minibutton js-update-url-with-hash">Blame</a>
          <a href="/farhadi/html5sortable/commits/master/jquery.sortable.js" class="button minibutton " rel="nofollow">History</a>
        </div><!-- /.button-group -->
          <a class="minibutton danger disabled empty-icon tooltipped tooltipped-w" href="#"
             aria-label="You must be signed in to make or propose changes">
          Delete
        </a>
      </div><!-- /.actions -->
    </div>
        <div class="blob-wrapper data type-javascript js-blob-data">
        <table class="file-code file-diff tab-size-8">
          <tr class="file-code-line">
            <td class="blob-line-nums">
              <span id="L1" rel="#L1">1</span>
<span id="L2" rel="#L2">2</span>
<span id="L3" rel="#L3">3</span>
<span id="L4" rel="#L4">4</span>
<span id="L5" rel="#L5">5</span>
<span id="L6" rel="#L6">6</span>
<span id="L7" rel="#L7">7</span>
<span id="L8" rel="#L8">8</span>
<span id="L9" rel="#L9">9</span>
<span id="L10" rel="#L10">10</span>
<span id="L11" rel="#L11">11</span>
<span id="L12" rel="#L12">12</span>
<span id="L13" rel="#L13">13</span>
<span id="L14" rel="#L14">14</span>
<span id="L15" rel="#L15">15</span>
<span id="L16" rel="#L16">16</span>
<span id="L17" rel="#L17">17</span>
<span id="L18" rel="#L18">18</span>
<span id="L19" rel="#L19">19</span>
<span id="L20" rel="#L20">20</span>
<span id="L21" rel="#L21">21</span>
<span id="L22" rel="#L22">22</span>
<span id="L23" rel="#L23">23</span>
<span id="L24" rel="#L24">24</span>
<span id="L25" rel="#L25">25</span>
<span id="L26" rel="#L26">26</span>
<span id="L27" rel="#L27">27</span>
<span id="L28" rel="#L28">28</span>
<span id="L29" rel="#L29">29</span>
<span id="L30" rel="#L30">30</span>
<span id="L31" rel="#L31">31</span>
<span id="L32" rel="#L32">32</span>
<span id="L33" rel="#L33">33</span>
<span id="L34" rel="#L34">34</span>
<span id="L35" rel="#L35">35</span>
<span id="L36" rel="#L36">36</span>
<span id="L37" rel="#L37">37</span>
<span id="L38" rel="#L38">38</span>
<span id="L39" rel="#L39">39</span>
<span id="L40" rel="#L40">40</span>
<span id="L41" rel="#L41">41</span>
<span id="L42" rel="#L42">42</span>
<span id="L43" rel="#L43">43</span>
<span id="L44" rel="#L44">44</span>
<span id="L45" rel="#L45">45</span>
<span id="L46" rel="#L46">46</span>
<span id="L47" rel="#L47">47</span>
<span id="L48" rel="#L48">48</span>
<span id="L49" rel="#L49">49</span>
<span id="L50" rel="#L50">50</span>
<span id="L51" rel="#L51">51</span>
<span id="L52" rel="#L52">52</span>
<span id="L53" rel="#L53">53</span>
<span id="L54" rel="#L54">54</span>
<span id="L55" rel="#L55">55</span>
<span id="L56" rel="#L56">56</span>
<span id="L57" rel="#L57">57</span>
<span id="L58" rel="#L58">58</span>
<span id="L59" rel="#L59">59</span>
<span id="L60" rel="#L60">60</span>
<span id="L61" rel="#L61">61</span>
<span id="L62" rel="#L62">62</span>
<span id="L63" rel="#L63">63</span>
<span id="L64" rel="#L64">64</span>
<span id="L65" rel="#L65">65</span>
<span id="L66" rel="#L66">66</span>
<span id="L67" rel="#L67">67</span>
<span id="L68" rel="#L68">68</span>
<span id="L69" rel="#L69">69</span>
<span id="L70" rel="#L70">70</span>
<span id="L71" rel="#L71">71</span>
<span id="L72" rel="#L72">72</span>
<span id="L73" rel="#L73">73</span>
<span id="L74" rel="#L74">74</span>
<span id="L75" rel="#L75">75</span>
<span id="L76" rel="#L76">76</span>
<span id="L77" rel="#L77">77</span>
<span id="L78" rel="#L78">78</span>
<span id="L79" rel="#L79">79</span>
<span id="L80" rel="#L80">80</span>
<span id="L81" rel="#L81">81</span>
<span id="L82" rel="#L82">82</span>
<span id="L83" rel="#L83">83</span>
<span id="L84" rel="#L84">84</span>
<span id="L85" rel="#L85">85</span>

            </td>
            <td class="blob-line-code"><div class="code-body highlight"><pre><div class='line' id='LC1'><span class="cm">/*</span></div><div class='line' id='LC2'><span class="cm"> * HTML5 Sortable jQuery Plugin</span></div><div class='line' id='LC3'><span class="cm"> * http://farhadi.ir/projects/html5sortable</span></div><div class='line' id='LC4'><span class="cm"> * </span></div><div class='line' id='LC5'><span class="cm"> * Copyright 2012, Ali Farhadi</span></div><div class='line' id='LC6'><span class="cm"> * Released under the MIT license.</span></div><div class='line' id='LC7'><span class="cm"> */</span></div><div class='line' id='LC8'><span class="p">(</span><span class="kd">function</span><span class="p">(</span><span class="nx">$</span><span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC9'><span class="kd">var</span> <span class="nx">dragging</span><span class="p">,</span> <span class="nx">placeholders</span> <span class="o">=</span> <span class="nx">$</span><span class="p">();</span></div><div class='line' id='LC10'><span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">sortable</span> <span class="o">=</span> <span class="kd">function</span><span class="p">(</span><span class="nx">options</span><span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC11'>	<span class="kd">var</span> <span class="nx">method</span> <span class="o">=</span> <span class="nb">String</span><span class="p">(</span><span class="nx">options</span><span class="p">);</span></div><div class='line' id='LC12'>	<span class="nx">options</span> <span class="o">=</span> <span class="nx">$</span><span class="p">.</span><span class="nx">extend</span><span class="p">({</span></div><div class='line' id='LC13'>		<span class="nx">connectWith</span><span class="o">:</span> <span class="kc">false</span></div><div class='line' id='LC14'>	<span class="p">},</span> <span class="nx">options</span><span class="p">);</span></div><div class='line' id='LC15'>	<span class="k">return</span> <span class="k">this</span><span class="p">.</span><span class="nx">each</span><span class="p">(</span><span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC16'>		<span class="k">if</span> <span class="p">(</span><span class="sr">/^enable|disable|destroy$/</span><span class="p">.</span><span class="nx">test</span><span class="p">(</span><span class="nx">method</span><span class="p">))</span> <span class="p">{</span></div><div class='line' id='LC17'>			<span class="kd">var</span> <span class="nx">items</span> <span class="o">=</span> <span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">children</span><span class="p">(</span><span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">data</span><span class="p">(</span><span class="s1">&#39;items&#39;</span><span class="p">)).</span><span class="nx">attr</span><span class="p">(</span><span class="s1">&#39;draggable&#39;</span><span class="p">,</span> <span class="nx">method</span> <span class="o">==</span> <span class="s1">&#39;enable&#39;</span><span class="p">);</span></div><div class='line' id='LC18'>			<span class="k">if</span> <span class="p">(</span><span class="nx">method</span> <span class="o">==</span> <span class="s1">&#39;destroy&#39;</span><span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC19'>				<span class="nx">items</span><span class="p">.</span><span class="nx">add</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">removeData</span><span class="p">(</span><span class="s1">&#39;connectWith items&#39;</span><span class="p">)</span></div><div class='line' id='LC20'>					<span class="p">.</span><span class="nx">off</span><span class="p">(</span><span class="s1">&#39;dragstart.h5s dragend.h5s selectstart.h5s dragover.h5s dragenter.h5s drop.h5s&#39;</span><span class="p">);</span></div><div class='line' id='LC21'>			<span class="p">}</span></div><div class='line' id='LC22'>			<span class="k">return</span><span class="p">;</span></div><div class='line' id='LC23'>		<span class="p">}</span></div><div class='line' id='LC24'>		<span class="kd">var</span> <span class="nx">isHandle</span><span class="p">,</span> <span class="nx">index</span><span class="p">,</span> <span class="nx">items</span> <span class="o">=</span> <span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">children</span><span class="p">(</span><span class="nx">options</span><span class="p">.</span><span class="nx">items</span><span class="p">);</span></div><div class='line' id='LC25'>		<span class="kd">var</span> <span class="nx">placeholder</span> <span class="o">=</span> <span class="nx">$</span><span class="p">(</span><span class="s1">&#39;&lt;&#39;</span> <span class="o">+</span> <span class="p">(</span><span class="sr">/^ul|ol$/i</span><span class="p">.</span><span class="nx">test</span><span class="p">(</span><span class="k">this</span><span class="p">.</span><span class="nx">tagName</span><span class="p">)</span> <span class="o">?</span> <span class="s1">&#39;li&#39;</span> <span class="o">:</span> <span class="s1">&#39;div&#39;</span><span class="p">)</span> <span class="o">+</span> <span class="s1">&#39; class=&quot;sortable-placeholder&quot;&gt;&#39;</span><span class="p">);</span></div><div class='line' id='LC26'>		<span class="nx">items</span><span class="p">.</span><span class="nx">find</span><span class="p">(</span><span class="nx">options</span><span class="p">.</span><span class="nx">handle</span><span class="p">).</span><span class="nx">mousedown</span><span class="p">(</span><span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC27'>			<span class="nx">isHandle</span> <span class="o">=</span> <span class="kc">true</span><span class="p">;</span></div><div class='line' id='LC28'>		<span class="p">}).</span><span class="nx">mouseup</span><span class="p">(</span><span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC29'>			<span class="nx">isHandle</span> <span class="o">=</span> <span class="kc">false</span><span class="p">;</span></div><div class='line' id='LC30'>		<span class="p">});</span></div><div class='line' id='LC31'>		<span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">data</span><span class="p">(</span><span class="s1">&#39;items&#39;</span><span class="p">,</span> <span class="nx">options</span><span class="p">.</span><span class="nx">items</span><span class="p">)</span></div><div class='line' id='LC32'>		<span class="nx">placeholders</span> <span class="o">=</span> <span class="nx">placeholders</span><span class="p">.</span><span class="nx">add</span><span class="p">(</span><span class="nx">placeholder</span><span class="p">);</span></div><div class='line' id='LC33'>		<span class="k">if</span> <span class="p">(</span><span class="nx">options</span><span class="p">.</span><span class="nx">connectWith</span><span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC34'>			<span class="nx">$</span><span class="p">(</span><span class="nx">options</span><span class="p">.</span><span class="nx">connectWith</span><span class="p">).</span><span class="nx">add</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">data</span><span class="p">(</span><span class="s1">&#39;connectWith&#39;</span><span class="p">,</span> <span class="nx">options</span><span class="p">.</span><span class="nx">connectWith</span><span class="p">);</span></div><div class='line' id='LC35'>		<span class="p">}</span></div><div class='line' id='LC36'>		<span class="nx">items</span><span class="p">.</span><span class="nx">attr</span><span class="p">(</span><span class="s1">&#39;draggable&#39;</span><span class="p">,</span> <span class="s1">&#39;true&#39;</span><span class="p">).</span><span class="nx">on</span><span class="p">(</span><span class="s1">&#39;dragstart.h5s&#39;</span><span class="p">,</span> <span class="kd">function</span><span class="p">(</span><span class="nx">e</span><span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC37'>			<span class="k">if</span> <span class="p">(</span><span class="nx">options</span><span class="p">.</span><span class="nx">handle</span> <span class="o">&amp;&amp;</span> <span class="o">!</span><span class="nx">isHandle</span><span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC38'>				<span class="k">return</span> <span class="kc">false</span><span class="p">;</span></div><div class='line' id='LC39'>			<span class="p">}</span></div><div class='line' id='LC40'>			<span class="nx">isHandle</span> <span class="o">=</span> <span class="kc">false</span><span class="p">;</span></div><div class='line' id='LC41'>			<span class="kd">var</span> <span class="nx">dt</span> <span class="o">=</span> <span class="nx">e</span><span class="p">.</span><span class="nx">originalEvent</span><span class="p">.</span><span class="nx">dataTransfer</span><span class="p">;</span></div><div class='line' id='LC42'>			<span class="nx">dt</span><span class="p">.</span><span class="nx">effectAllowed</span> <span class="o">=</span> <span class="s1">&#39;move&#39;</span><span class="p">;</span></div><div class='line' id='LC43'>			<span class="nx">dt</span><span class="p">.</span><span class="nx">setData</span><span class="p">(</span><span class="s1">&#39;Text&#39;</span><span class="p">,</span> <span class="s1">&#39;dummy&#39;</span><span class="p">);</span></div><div class='line' id='LC44'>			<span class="nx">index</span> <span class="o">=</span> <span class="p">(</span><span class="nx">dragging</span> <span class="o">=</span> <span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">)).</span><span class="nx">addClass</span><span class="p">(</span><span class="s1">&#39;sortable-dragging&#39;</span><span class="p">).</span><span class="nx">index</span><span class="p">();</span></div><div class='line' id='LC45'>		<span class="p">}).</span><span class="nx">on</span><span class="p">(</span><span class="s1">&#39;dragend.h5s&#39;</span><span class="p">,</span> <span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC46'>			<span class="k">if</span> <span class="p">(</span><span class="o">!</span><span class="nx">dragging</span><span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC47'>				<span class="k">return</span><span class="p">;</span></div><div class='line' id='LC48'>			<span class="p">}</span></div><div class='line' id='LC49'>			<span class="nx">dragging</span><span class="p">.</span><span class="nx">removeClass</span><span class="p">(</span><span class="s1">&#39;sortable-dragging&#39;</span><span class="p">).</span><span class="nx">show</span><span class="p">();</span></div><div class='line' id='LC50'>			<span class="nx">placeholders</span><span class="p">.</span><span class="nx">detach</span><span class="p">();</span></div><div class='line' id='LC51'>			<span class="k">if</span> <span class="p">(</span><span class="nx">index</span> <span class="o">!=</span> <span class="nx">dragging</span><span class="p">.</span><span class="nx">index</span><span class="p">())</span> <span class="p">{</span></div><div class='line' id='LC52'>				<span class="nx">dragging</span><span class="p">.</span><span class="nx">parent</span><span class="p">().</span><span class="nx">trigger</span><span class="p">(</span><span class="s1">&#39;sortupdate&#39;</span><span class="p">,</span> <span class="p">{</span><span class="nx">item</span><span class="o">:</span> <span class="nx">dragging</span><span class="p">});</span></div><div class='line' id='LC53'>			<span class="p">}</span></div><div class='line' id='LC54'>			<span class="nx">dragging</span> <span class="o">=</span> <span class="kc">null</span><span class="p">;</span></div><div class='line' id='LC55'>		<span class="p">}).</span><span class="nx">not</span><span class="p">(</span><span class="s1">&#39;a[href], img&#39;</span><span class="p">).</span><span class="nx">on</span><span class="p">(</span><span class="s1">&#39;selectstart.h5s&#39;</span><span class="p">,</span> <span class="kd">function</span><span class="p">()</span> <span class="p">{</span></div><div class='line' id='LC56'>			<span class="k">this</span><span class="p">.</span><span class="nx">dragDrop</span> <span class="o">&amp;&amp;</span> <span class="k">this</span><span class="p">.</span><span class="nx">dragDrop</span><span class="p">();</span></div><div class='line' id='LC57'>			<span class="k">return</span> <span class="kc">false</span><span class="p">;</span></div><div class='line' id='LC58'>		<span class="p">}).</span><span class="nx">end</span><span class="p">().</span><span class="nx">add</span><span class="p">([</span><span class="k">this</span><span class="p">,</span> <span class="nx">placeholder</span><span class="p">]).</span><span class="nx">on</span><span class="p">(</span><span class="s1">&#39;dragover.h5s dragenter.h5s drop.h5s&#39;</span><span class="p">,</span> <span class="kd">function</span><span class="p">(</span><span class="nx">e</span><span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC59'>			<span class="k">if</span> <span class="p">(</span><span class="o">!</span><span class="nx">items</span><span class="p">.</span><span class="nx">is</span><span class="p">(</span><span class="nx">dragging</span><span class="p">)</span> <span class="o">&amp;&amp;</span> <span class="nx">options</span><span class="p">.</span><span class="nx">connectWith</span> <span class="o">!==</span> <span class="nx">$</span><span class="p">(</span><span class="nx">dragging</span><span class="p">).</span><span class="nx">parent</span><span class="p">().</span><span class="nx">data</span><span class="p">(</span><span class="s1">&#39;connectWith&#39;</span><span class="p">))</span> <span class="p">{</span></div><div class='line' id='LC60'>				<span class="k">return</span> <span class="kc">true</span><span class="p">;</span></div><div class='line' id='LC61'>			<span class="p">}</span></div><div class='line' id='LC62'>			<span class="k">if</span> <span class="p">(</span><span class="nx">e</span><span class="p">.</span><span class="nx">type</span> <span class="o">==</span> <span class="s1">&#39;drop&#39;</span><span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC63'>				<span class="nx">e</span><span class="p">.</span><span class="nx">stopPropagation</span><span class="p">();</span></div><div class='line' id='LC64'>				<span class="nx">placeholders</span><span class="p">.</span><span class="nx">filter</span><span class="p">(</span><span class="s1">&#39;:visible&#39;</span><span class="p">).</span><span class="nx">after</span><span class="p">(</span><span class="nx">dragging</span><span class="p">);</span></div><div class='line' id='LC65'>				<span class="nx">dragging</span><span class="p">.</span><span class="nx">trigger</span><span class="p">(</span><span class="s1">&#39;dragend.h5s&#39;</span><span class="p">);</span></div><div class='line' id='LC66'>				<span class="k">return</span> <span class="kc">false</span><span class="p">;</span></div><div class='line' id='LC67'>			<span class="p">}</span></div><div class='line' id='LC68'>			<span class="nx">e</span><span class="p">.</span><span class="nx">preventDefault</span><span class="p">();</span></div><div class='line' id='LC69'>			<span class="nx">e</span><span class="p">.</span><span class="nx">originalEvent</span><span class="p">.</span><span class="nx">dataTransfer</span><span class="p">.</span><span class="nx">dropEffect</span> <span class="o">=</span> <span class="s1">&#39;move&#39;</span><span class="p">;</span></div><div class='line' id='LC70'>			<span class="k">if</span> <span class="p">(</span><span class="nx">items</span><span class="p">.</span><span class="nx">is</span><span class="p">(</span><span class="k">this</span><span class="p">))</span> <span class="p">{</span></div><div class='line' id='LC71'>				<span class="k">if</span> <span class="p">(</span><span class="nx">options</span><span class="p">.</span><span class="nx">forcePlaceholderSize</span><span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC72'>					<span class="nx">placeholder</span><span class="p">.</span><span class="nx">height</span><span class="p">(</span><span class="nx">dragging</span><span class="p">.</span><span class="nx">outerHeight</span><span class="p">());</span></div><div class='line' id='LC73'>				<span class="p">}</span></div><div class='line' id='LC74'>				<span class="nx">dragging</span><span class="p">.</span><span class="nx">hide</span><span class="p">();</span></div><div class='line' id='LC75'>				<span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">)[</span><span class="nx">placeholder</span><span class="p">.</span><span class="nx">index</span><span class="p">()</span> <span class="o">&lt;</span> <span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">index</span><span class="p">()</span> <span class="o">?</span> <span class="s1">&#39;after&#39;</span> <span class="o">:</span> <span class="s1">&#39;before&#39;</span><span class="p">](</span><span class="nx">placeholder</span><span class="p">);</span></div><div class='line' id='LC76'>				<span class="nx">placeholders</span><span class="p">.</span><span class="nx">not</span><span class="p">(</span><span class="nx">placeholder</span><span class="p">).</span><span class="nx">detach</span><span class="p">();</span></div><div class='line' id='LC77'>			<span class="p">}</span> <span class="k">else</span> <span class="k">if</span> <span class="p">(</span><span class="o">!</span><span class="nx">placeholders</span><span class="p">.</span><span class="nx">is</span><span class="p">(</span><span class="k">this</span><span class="p">)</span> <span class="o">&amp;&amp;</span> <span class="o">!</span><span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">children</span><span class="p">(</span><span class="nx">options</span><span class="p">.</span><span class="nx">items</span><span class="p">).</span><span class="nx">length</span><span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC78'>				<span class="nx">placeholders</span><span class="p">.</span><span class="nx">detach</span><span class="p">();</span></div><div class='line' id='LC79'>				<span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">append</span><span class="p">(</span><span class="nx">placeholder</span><span class="p">);</span></div><div class='line' id='LC80'>			<span class="p">}</span></div><div class='line' id='LC81'>			<span class="k">return</span> <span class="kc">false</span><span class="p">;</span></div><div class='line' id='LC82'>		<span class="p">});</span></div><div class='line' id='LC83'>	<span class="p">});</span></div><div class='line' id='LC84'><span class="p">};</span></div><div class='line' id='LC85'><span class="p">})(</span><span class="nx">jQuery</span><span class="p">);</span></div></pre></div></td>
          </tr>
        </table>
  </div>

  </div>
</div>

<a href="#jump-to-line" rel="facebox[.linejump]" data-hotkey="l" class="js-jump-to-line" style="display:none">Jump to Line</a>
<div id="jump-to-line" style="display:none">
  <form accept-charset="UTF-8" class="js-jump-to-line-form">
    <input class="linejump-input js-jump-to-line-field" type="text" placeholder="Jump to line&hellip;" autofocus>
    <button type="submit" class="button">Go</button>
  </form>
</div>

        </div>

      </div><!-- /.repo-container -->
      <div class="modal-backdrop"></div>
    </div><!-- /.container -->
  </div><!-- /.site -->


    </div><!-- /.wrapper -->

      <div class="container">
  <div class="site-footer">
    <ul class="site-footer-links right">
      <li><a href="https://status.github.com/">Status</a></li>
      <li><a href="http://developer.github.com">API</a></li>
      <li><a href="http://training.github.com">Training</a></li>
      <li><a href="http://shop.github.com">Shop</a></li>
      <li><a href="/blog">Blog</a></li>
      <li><a href="/about">About</a></li>

    </ul>

    <a href="/">
      <span class="mega-octicon octicon-mark-github" title="GitHub"></span>
    </a>

    <ul class="site-footer-links">
      <li>&copy; 2014 <span title="0.02918s from github-fe140-cp1-prd.iad.github.net">GitHub</span>, Inc.</li>
        <li><a href="/site/terms">Terms</a></li>
        <li><a href="/site/privacy">Privacy</a></li>
        <li><a href="/security">Security</a></li>
        <li><a href="/contact">Contact</a></li>
    </ul>
  </div><!-- /.site-footer -->
</div><!-- /.container -->


    <div class="fullscreen-overlay js-fullscreen-overlay" id="fullscreen_overlay">
  <div class="fullscreen-container js-fullscreen-container">
    <div class="textarea-wrap">
      <textarea name="fullscreen-contents" id="fullscreen-contents" class="fullscreen-contents js-fullscreen-contents" placeholder="" data-suggester="fullscreen_suggester"></textarea>
    </div>
  </div>
  <div class="fullscreen-sidebar">
    <a href="#" class="exit-fullscreen js-exit-fullscreen tooltipped tooltipped-w" aria-label="Exit Zen Mode">
      <span class="mega-octicon octicon-screen-normal"></span>
    </a>
    <a href="#" class="theme-switcher js-theme-switcher tooltipped tooltipped-w"
      aria-label="Switch themes">
      <span class="octicon octicon-color-mode"></span>
    </a>
  </div>
</div>



    <div id="ajax-error-message" class="flash flash-error">
      <span class="octicon octicon-alert"></span>
      <a href="#" class="octicon octicon-remove-close close js-ajax-error-dismiss"></a>
      Something went wrong with that request. Please try again.
    </div>

  </body>
</html>

