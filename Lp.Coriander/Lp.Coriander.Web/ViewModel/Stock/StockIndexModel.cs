﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lp.Coriander.Web.ViewModel.Stock
{
    public class StockIndexModel
    {
        public StockIndexModel()
        {
           StockIndexItemModels = new List<StockIndexItemModel>();
        }
        public List<StockIndexItemModel> StockIndexItemModels { get; set; }
    }

    public class StockIndexItemModel
    {
        public int ItemId { get; set; }
        public string ItemCode { get; set; }
        public string Description { get; set; }

    }
}