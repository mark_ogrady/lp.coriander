﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lp.Coriander.Web.ViewModel.Dashboard
{
    public class DashboardIndexModel
    {
        public DashboardIndexModel()
        {
            DashboardIndexItemModels = new List<DashboardIndexItemModel>();
        }

        public List<DashboardIndexItemModel> DashboardIndexItemModels { get; set; }
    }

    public class DashboardIndexItemModel
    {
        public int SequenceNo { get; set; }
        public int Count { get; set; }

    }
}