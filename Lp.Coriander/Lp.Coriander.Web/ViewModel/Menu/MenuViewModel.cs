﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lp.Coriander.Web.ViewModel.Menu
{
    public class MenuViewModel
    {
       public string SelectedMenu { get; set; }
        public IEnumerable<Lp.Coriander.Framework.Model.Menu> Menus { get; set; }
    }
}