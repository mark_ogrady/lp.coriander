﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lp.Coriander.Web.ViewModel
{
    public interface IViewModel
    {
        ScreenType ScreenType { get; set; }
    }
}