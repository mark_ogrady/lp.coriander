﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lp.Coriander.Web.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ManageProfile()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult Notifications()
        {
            return PartialView("User/Notifications");
        }

        [ChildActionOnly]
        public ActionResult UserInfo()
        {
            return PartialView("User/UserInfo");
        }


    }
}