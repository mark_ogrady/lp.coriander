﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lp.Coriander.Framework.Data;
using Lp.Coriander.Web.ViewModel.Stock;

namespace Lp.Coriander.Web.Controllers
{
    public class StockController : Controller
    {
        private LpContext _context = new LpContext();
        private Sage _sageContext = new Sage();
        // GET: Stock
        public ActionResult Index()
        {
            var model = new StockIndexModel();

            var stockItems = _sageContext.StockItems.ToList();
            foreach (var stockItem in stockItems)
            {
                model.StockIndexItemModels.Add(new StockIndexItemModel()
                {
                    Description = stockItem.Description,
                    ItemId = (int)stockItem.ItemID
                });
            }


            return View(model);
        }


        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }
    }
}