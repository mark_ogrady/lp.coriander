﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lp.Coriander.Web.Controllers
{
    public class MenuController : Controller
    {
        // GET: Menu
        [ChildActionOnly]
        public ActionResult AppSideMenu()
        {
            return PartialView("Menu/AppSideMenu");
        }
    }
}