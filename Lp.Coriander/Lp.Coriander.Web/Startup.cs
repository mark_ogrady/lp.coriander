﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Lp.Coriander.Web.Startup))]
namespace Lp.Coriander.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
