﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lp.Coriander.Framework.Model
{
    public class ApplicationLog
    {
        [Key]
        public Guid ApplicationLogId { get; set; }
        public DateTime CreationDate { get; set; }
        public string User { get; set; }
        public string Company { get; set; }
        public string Thread { get; set; }
        public string Level { get; set; }
        public string Logger { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public bool IsDismissed { get; set; }


    }
}
