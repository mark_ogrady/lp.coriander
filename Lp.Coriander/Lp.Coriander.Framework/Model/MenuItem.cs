﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lp.Coriander.Framework.Model
{
    public class MenuItem
    {
        [Key]
        public Guid MenuItemId { get; set; }
        public string Icon { get; set; }
        public string ItemName { get; set; }
        public string Link { get; set; }
        public int OrderPosition { get; set; }
        public virtual Menu QcMenu { get; set; }

    }
}
