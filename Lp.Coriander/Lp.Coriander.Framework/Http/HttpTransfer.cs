﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Lp.Coriander.Framework.Http
{
    public class HttpTransfer
    {
        public void Post<T>(string url, string postData,string username,string password, Action<T> onComplete,Action<Exception> onError)
        {
            using (var webClient = new WebClient())
            {

                webClient.UploadDataCompleted += (s, e) =>
                {
                    if (e.Error == null)
                    {
                        try
                        {
                            string response = Encoding.UTF8.GetString(e.Result);
                            T obj = JsonConvert.DeserializeObject<T>(response);
                            onComplete((T)obj);

                        }
                        catch (Exception ex)
                        {

                           onError(new JsonException(ex.Message));
                        }
                      

                    }
                    else
                    {
                        onError(e.Error);
                    }

                };
                webClient.Headers.Add("Content-Type", "application/json");
                webClient.Encoding = Encoding.UTF8;
                if (username != null && password != null)
                {
                    webClient.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(username + ":" + password)));
                }
                byte[] data = Encoding.UTF8.GetBytes(postData);
                webClient.UploadDataAsync(new Uri(url), "POST", data);

            }

           

        }
   

    
}
    }



