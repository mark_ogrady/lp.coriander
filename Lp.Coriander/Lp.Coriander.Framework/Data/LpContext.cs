﻿using System.Data.Entity;
using System.Security.AccessControl;
using Lp.Coriander.Framework.Model;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lp.Coriander.Framework.Data
{
    public class LpContext : IdentityDbContext<ApplicationUser>
    {
        public LpContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            
        }

        public static LpContext Create()
        {
            return new LpContext();
        }


        public DbSet<ApplicationLog> ApplicationLogs { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<MenuItem> MenuItems { get; set; }
        public DbSet<DashboardItem> DashboardItems { get; set; }
    }
}
