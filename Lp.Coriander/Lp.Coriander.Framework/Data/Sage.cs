namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Sage : DbContext
    {
        public Sage()
            : base("name=Sage")
        {
        }

        public virtual DbSet<AlternativeItem> AlternativeItems { get; set; }
        public virtual DbSet<SLCustomerAccount> SLCustomerAccounts { get; set; }
        public virtual DbSet<SLCustomerContact> SLCustomerContacts { get; set; }
        public virtual DbSet<StockItem> StockItems { get; set; }
        public virtual DbSet<StockItemNominalCode> StockItemNominalCodes { get; set; }
        public virtual DbSet<StockItemPrice> StockItemPrices { get; set; }
        public virtual DbSet<StockItemQtyDiscBreak> StockItemQtyDiscBreaks { get; set; }
        public virtual DbSet<StockItemStatu> StockItemStatus { get; set; }
        public virtual DbSet<StockItemSupplier> StockItemSuppliers { get; set; }
        public virtual DbSet<StockItemType> StockItemTypes { get; set; }
        public virtual DbSet<StockItemUnit> StockItemUnits { get; set; }
        public virtual DbSet<StockItemUnitUOMType> StockItemUnitUOMTypes { get; set; }
        public virtual DbSet<StockSetting> StockSettings { get; set; }
        public virtual DbSet<Stocktake> Stocktakes { get; set; }
        public virtual DbSet<StocktakeCountSheetItem> StocktakeCountSheetItems { get; set; }
        public virtual DbSet<StocktakeItem> StocktakeItems { get; set; }
        public virtual DbSet<StocktakeLiveBinItem> StocktakeLiveBinItems { get; set; }
        public virtual DbSet<StocktakeSelector> StocktakeSelectors { get; set; }
        public virtual DbSet<StocktakeStatu> StocktakeStatus { get; set; }
        public virtual DbSet<StocktakeType> StocktakeTypes { get; set; }
        public virtual DbSet<Warehouse> Warehouses { get; set; }
        public virtual DbSet<WarehouseItem> WarehouseItems { get; set; }
        public virtual DbSet<WarehouseType> WarehouseTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AlternativeItem>()
                .Property(e => e.OpLock)
                .IsFixedLength();

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.CustomerAccountNumber)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.CustomerAccountName)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.CustomerAccountShortName)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.TaxRegistrationNumber)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.DefaultOrderPriority)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.DefaultNominalAccountNumber)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.DefaultNominalCostCentre)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.DefaultNominalDepartment)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.InvoiceDiscountPercent)
                .HasPrecision(6, 2);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.EuroAccountNumberCopiedFromTo)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.OpLock)
                .IsFixedLength();

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.TradingTerms)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.CreditReference)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.SpareText1)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.SpareText2)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.SpareText3)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.SpareNumber1)
                .HasPrecision(18, 5);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.SpareNumber2)
                .HasPrecision(18, 5);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.SpareNumber3)
                .HasPrecision(18, 5);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.DUNSCode)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.MainTelephoneAreaCode)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.MainTelephoneCountryCode)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.MainTelephoneSubscriberNumber)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.MainFaxAreaCode)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.MainFaxCountryCode)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.MainFaxSubscriberNumber)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.MainWebsite)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode1)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode2)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode3)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode4)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode5)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode6)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode7)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode8)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode9)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode10)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode11)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode12)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode13)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode14)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode15)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode16)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode17)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode18)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode19)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .Property(e => e.AnalysisCode20)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerAccount>()
                .HasMany(e => e.SLCustomerAccount1)
                .WithOptional(e => e.SLCustomerAccount2)
                .HasForeignKey(e => e.AssociatedHeadOfficeAccountID);

            modelBuilder.Entity<SLCustomerAccount>()
                .HasMany(e => e.SLCustomerContacts)
                .WithRequired(e => e.SLCustomerAccount)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SLCustomerContact>()
                .Property(e => e.ContactName)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerContact>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerContact>()
                .Property(e => e.OpLock)
                .IsFixedLength();

            modelBuilder.Entity<SLCustomerContact>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerContact>()
                .Property(e => e.MiddleName)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerContact>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<SLCustomerContact>()
                .Property(e => e.ContactNamePreMigratedData)
                .IsUnicode(false);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.StandardCost)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.SOPItemPrice)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.Manufacturer)
                .IsUnicode(false);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.PartNumber)
                .IsUnicode(false);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.CommodityCode)
                .IsUnicode(false);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.Weight)
                .HasPrecision(15, 4);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.StockUnitName)
                .IsUnicode(false);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.BaseUnitName)
                .IsUnicode(false);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.StockMultOfBaseUnit)
                .HasPrecision(18, 9);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.Barcode)
                .IsUnicode(false);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.AverageBuyingPrice)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.FreeStockQuantity)
                .HasPrecision(15, 5);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.DefaultDespatchNoteComment)
                .IsUnicode(false);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.DefaultPickingListComment)
                .IsUnicode(false);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.QuantityReserved)
                .HasPrecision(15, 5);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.OpLock)
                .IsFixedLength();

            modelBuilder.Entity<StockItem>()
                .Property(e => e.LandedCostsValue)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.SpareText1)
                .IsUnicode(false);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.SpareText2)
                .IsUnicode(false);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.SpareText3)
                .IsUnicode(false);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.SpareNumber1)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.SpareNumber2)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.SpareNumber3)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.AutoGeneratePrefix)
                .IsUnicode(false);

            modelBuilder.Entity<StockItem>()
                .Property(e => e.SupplementaryUnitConversionRatio)
                .HasPrecision(18, 9);

            modelBuilder.Entity<StockItem>()
                .HasMany(e => e.AlternativeItems)
                .WithRequired(e => e.StockItem)
                .HasForeignKey(e => e.ItemID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StockItem>()
                .HasMany(e => e.AlternativeItems1)
                .WithRequired(e => e.StockItem1)
                .HasForeignKey(e => e.ItemAlternativeID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StockItem>()
                .HasMany(e => e.StockItemNominalCodes)
                .WithRequired(e => e.StockItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StockItem>()
                .HasMany(e => e.StockItemPrices)
                .WithRequired(e => e.StockItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StockItem>()
                .HasMany(e => e.StockItemSuppliers)
                .WithRequired(e => e.StockItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StockItem>()
                .HasMany(e => e.StockItemUnits)
                .WithRequired(e => e.StockItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StockItem>()
                .HasMany(e => e.StockItemUnitUOMTypes)
                .WithRequired(e => e.StockItem)
                .HasForeignKey(e => e.StockItemID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StockItem>()
                .HasMany(e => e.WarehouseItems)
                .WithRequired(e => e.StockItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StockItemNominalCode>()
                .Property(e => e.OpLock)
                .IsFixedLength();

            modelBuilder.Entity<StockItemPrice>()
                .Property(e => e.Price)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StockItemPrice>()
                .Property(e => e.OpLock)
                .IsFixedLength();

            modelBuilder.Entity<StockItemQtyDiscBreak>()
                .Property(e => e.QuantityMoreThan)
                .HasPrecision(15, 5);

            modelBuilder.Entity<StockItemQtyDiscBreak>()
                .Property(e => e.DiscountAmountValue)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StockItemQtyDiscBreak>()
                .Property(e => e.DiscountPercentValue)
                .HasPrecision(6, 2);

            modelBuilder.Entity<StockItemQtyDiscBreak>()
                .Property(e => e.OpLock)
                .IsFixedLength();

            modelBuilder.Entity<StockItemStatu>()
                .Property(e => e.StockItemStatusName)
                .IsUnicode(false);

            modelBuilder.Entity<StockItemSupplier>()
                .Property(e => e.SupplierStockCode)
                .IsUnicode(false);

            modelBuilder.Entity<StockItemSupplier>()
                .Property(e => e.UsualOrderQuantity)
                .HasPrecision(15, 5);

            modelBuilder.Entity<StockItemSupplier>()
                .Property(e => e.MinimumOrderQuantity)
                .HasPrecision(15, 5);

            modelBuilder.Entity<StockItemSupplier>()
                .Property(e => e.LastBuyingPrice)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StockItemSupplier>()
                .Property(e => e.LastOrderQuantity)
                .HasPrecision(15, 5);

            modelBuilder.Entity<StockItemSupplier>()
                .Property(e => e.OrderQuantityYTD)
                .HasPrecision(15, 5);

            modelBuilder.Entity<StockItemSupplier>()
                .Property(e => e.OrderValueYTD)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StockItemSupplier>()
                .Property(e => e.QuantityOnOrder)
                .HasPrecision(15, 5);

            modelBuilder.Entity<StockItemSupplier>()
                .Property(e => e.LastBaseBuyingPrice)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StockItemSupplier>()
                .Property(e => e.CataloguePrice)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StockItemSupplier>()
                .Property(e => e.OpLock)
                .IsFixedLength();

            modelBuilder.Entity<StockItemSupplier>()
                .Property(e => e.ListPrice)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StockItemSupplier>()
                .Property(e => e.ListBasePrice)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StockItemSupplier>()
                .Property(e => e.LandedCostsValue)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StockItemSupplier>()
                .Property(e => e.ReorderMultipleQty)
                .HasPrecision(15, 5);

            modelBuilder.Entity<StockItemType>()
                .Property(e => e.StockItemTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<StockItemUnit>()
                .Property(e => e.MultipleOfBaseUnit)
                .HasPrecision(18, 9);

            modelBuilder.Entity<StockItemUnit>()
                .Property(e => e.OpLock)
                .IsFixedLength();

            modelBuilder.Entity<StockItemUnit>()
                .Property(e => e.UnitPrecision)
                .HasPrecision(15, 5);

            modelBuilder.Entity<StockItemUnit>()
                .HasMany(e => e.StockItemUnitUOMTypes)
                .WithRequired(e => e.StockItemUnit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StockItemUnitUOMType>()
                .Property(e => e.OpLock)
                .IsFixedLength();

            modelBuilder.Entity<StockSetting>()
                .Property(e => e.LastArchiveRunBy)
                .IsUnicode(false);

            modelBuilder.Entity<StockSetting>()
                .Property(e => e.LastPurgeArchiveRunBy)
                .IsUnicode(false);

            modelBuilder.Entity<StockSetting>()
                .Property(e => e.LastProveBalancesRunBy)
                .IsUnicode(false);

            modelBuilder.Entity<StockSetting>()
                .Property(e => e.OpLock)
                .IsFixedLength();

            modelBuilder.Entity<StockSetting>()
                .Property(e => e.LastTraceArchiveRunBy)
                .IsUnicode(false);

            modelBuilder.Entity<StockSetting>()
                .Property(e => e.LastTracePurgeArchiveRunBy)
                .IsUnicode(false);

            modelBuilder.Entity<Stocktake>()
                .Property(e => e.NominalNarrative)
                .IsUnicode(false);

            modelBuilder.Entity<Stocktake>()
                .Property(e => e.OpLock)
                .IsFixedLength();

            modelBuilder.Entity<Stocktake>()
                .Property(e => e.StocktakeName)
                .IsUnicode(false);

            modelBuilder.Entity<Stocktake>()
                .HasMany(e => e.StocktakeCountSheetItems)
                .WithRequired(e => e.Stocktake)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Stocktake>()
                .HasMany(e => e.StocktakeItems)
                .WithRequired(e => e.Stocktake)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Stocktake>()
                .HasMany(e => e.StocktakeLiveBinItems)
                .WithRequired(e => e.Stocktake)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StocktakeCountSheetItem>()
                .Property(e => e.RecordedQuantityInStock)
                .HasPrecision(15, 5);

            modelBuilder.Entity<StocktakeCountSheetItem>()
                .Property(e => e.ActualQuantityInStock)
                .HasPrecision(15, 5);

            modelBuilder.Entity<StocktakeCountSheetItem>()
                .Property(e => e.RecordedTraceUnassigned)
                .HasPrecision(15, 5);

            modelBuilder.Entity<StocktakeCountSheetItem>()
                .Property(e => e.ActualTraceUnassigned)
                .HasPrecision(15, 5);

            modelBuilder.Entity<StocktakeCountSheetItem>()
                .Property(e => e.OpLock)
                .IsFixedLength();

            modelBuilder.Entity<StocktakeCountSheetItem>()
                .Property(e => e.QuantityOnPOPOrder)
                .HasPrecision(15, 5);

            modelBuilder.Entity<StocktakeCountSheetItem>()
                .Property(e => e.DiscrepancyNarrative)
                .IsUnicode(false);

            modelBuilder.Entity<StocktakeItem>()
                .Property(e => e.AverageBuyingPrice)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StocktakeItem>()
                .Property(e => e.HoldingValue)
                .HasPrecision(18, 5);

            modelBuilder.Entity<StocktakeItem>()
                .Property(e => e.OpLock)
                .IsFixedLength();

            modelBuilder.Entity<StocktakeItem>()
                .Property(e => e.Details)
                .IsUnicode(false);

            modelBuilder.Entity<StocktakeItem>()
                .HasMany(e => e.StocktakeLiveBinItems)
                .WithRequired(e => e.StocktakeItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StocktakeSelector>()
                .Property(e => e.StocktakeSelectorName)
                .IsUnicode(false);

            modelBuilder.Entity<StocktakeSelector>()
                .HasMany(e => e.Stocktakes)
                .WithRequired(e => e.StocktakeSelector)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StocktakeStatu>()
                .Property(e => e.StocktakeStatusName)
                .IsUnicode(false);

            modelBuilder.Entity<StocktakeStatu>()
                .HasMany(e => e.Stocktakes)
                .WithRequired(e => e.StocktakeStatu)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StocktakeType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<StocktakeType>()
                .HasMany(e => e.Stocktakes)
                .WithRequired(e => e.StocktakeType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.PostalName)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.AddressLine1)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.AddressLine2)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.AddressLine3)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.AddressLine4)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.PostCode)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.Contact)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.TelephoneNo)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.FaxNo)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.EmailAddress)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.WebAddress)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.OpLock)
                .IsFixedLength();

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.SpareText1)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.SpareText2)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.SpareText3)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.SpareNumber1)
                .HasPrecision(18, 5);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.SpareNumber2)
                .HasPrecision(18, 5);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.SpareNumber3)
                .HasPrecision(18, 5);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.County)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<Warehouse>()
                .HasMany(e => e.Stocktakes)
                .WithRequired(e => e.Warehouse)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Warehouse>()
                .HasMany(e => e.WarehouseItems)
                .WithRequired(e => e.Warehouse)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WarehouseItem>()
                .Property(e => e.ReorderLevel)
                .HasPrecision(15, 5);

            modelBuilder.Entity<WarehouseItem>()
                .Property(e => e.MinimumLevel)
                .HasPrecision(15, 5);

            modelBuilder.Entity<WarehouseItem>()
                .Property(e => e.MaximumLevel)
                .HasPrecision(15, 5);

            modelBuilder.Entity<WarehouseItem>()
                .Property(e => e.ConfirmedQtyInStock)
                .HasPrecision(15, 5);

            modelBuilder.Entity<WarehouseItem>()
                .Property(e => e.UnconfirmedQtyInStock)
                .HasPrecision(15, 5);

            modelBuilder.Entity<WarehouseItem>()
                .Property(e => e.QuantityAllocatedSOP)
                .HasPrecision(15, 5);

            modelBuilder.Entity<WarehouseItem>()
                .Property(e => e.QuantityAllocatedStock)
                .HasPrecision(15, 5);

            modelBuilder.Entity<WarehouseItem>()
                .Property(e => e.QuantityOnPOPOrder)
                .HasPrecision(15, 5);

            modelBuilder.Entity<WarehouseItem>()
                .Property(e => e.HoldingValueAtBuyPrice)
                .HasPrecision(18, 5);

            modelBuilder.Entity<WarehouseItem>()
                .Property(e => e.PreReceiptAllocationQty)
                .HasPrecision(15, 5);

            modelBuilder.Entity<WarehouseItem>()
                .Property(e => e.QuantityAllocatedBOM)
                .HasPrecision(15, 5);

            modelBuilder.Entity<WarehouseItem>()
                .Property(e => e.QuantityReserved)
                .HasPrecision(15, 5);

            modelBuilder.Entity<WarehouseItem>()
                .Property(e => e.OpLock)
                .IsFixedLength();

            modelBuilder.Entity<WarehouseType>()
                .Property(e => e.WarehouseTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<WarehouseType>()
                .HasMany(e => e.Warehouses)
                .WithRequired(e => e.WarehouseType)
                .WillCascadeOnDelete(false);
        }
    }
}
