﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lp.Coriander.Framework.SageInterface.SOP
{
    public interface ISageSop
    {
        int InsertSop(TransferModels.SOP.SopOrder sop);

        int UpdateSop(TransferModels.SOP.SopOrder sop);
    }
}
