﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net.Appender;
using log4net.Core;
using Lp.Coriander.Framework.Data;
using Lp.Coriander.Framework.Model;

namespace Lp.Coriander.Framework.Logging
{
    public class DatabaseLogAppender : AppenderSkeleton
    {
        protected override void Append(LoggingEvent loggingEvent)
        {
            var logProperties = loggingEvent.GetProperties();
            string user = logProperties.Contains("UserName") ? logProperties["UserName"].ToString() : string.Empty;
            string company = logProperties.Contains("CompanyName") ? logProperties["CompanyName"].ToString() : string.Empty;

            using (var context = new LpContext())
            {
                var applicationLog = new ApplicationLog();
                applicationLog.ApplicationLogId = Guid.NewGuid();
                applicationLog.CreationDate = DateTime.UtcNow;
                applicationLog.Exception = loggingEvent.GetExceptionString();
                applicationLog.Level = loggingEvent.Level.DisplayName;
                applicationLog.Thread = loggingEvent.ThreadName;
                applicationLog.Message = loggingEvent.RenderedMessage;
                applicationLog.User = user;
                applicationLog.Company = company;
                context.ApplicationLogs.Add(applicationLog);
                context.SaveChanges();

            }


        }
    }
}
