namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StocktakeCountSheetItem")]
    public partial class StocktakeCountSheetItem
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long StocktakeCountShtItemID { get; set; }

        public long BinItemID { get; set; }

        public long StocktakeID { get; set; }

        public decimal RecordedQuantityInStock { get; set; }

        public bool ActualQuantityEntered { get; set; }

        public decimal ActualQuantityInStock { get; set; }

        public decimal RecordedTraceUnassigned { get; set; }

        public decimal ActualTraceUnassigned { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] OpLock { get; set; }

        public DateTime DateTimeCreated { get; set; }

        public decimal QuantityOnPOPOrder { get; set; }

        public long STKDiscrepancyStatusID { get; set; }

        [Required]
        [StringLength(255)]
        public string DiscrepancyNarrative { get; set; }

        public virtual Stocktake Stocktake { get; set; }
    }
}
