namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StockItemUnit")]
    public partial class StockItemUnit
    {
        public StockItemUnit()
        {
            StockItemUnitUOMTypes = new HashSet<StockItemUnitUOMType>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long StockItemUnitID { get; set; }

        public long ItemID { get; set; }

        public long UnitID { get; set; }

        public decimal MultipleOfBaseUnit { get; set; }

        public bool UseOwnPrice { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] OpLock { get; set; }

        public DateTime DateTimeCreated { get; set; }

        public decimal UnitPrecision { get; set; }

        public virtual StockItem StockItem { get; set; }

        public virtual ICollection<StockItemUnitUOMType> StockItemUnitUOMTypes { get; set; }
    }
}
