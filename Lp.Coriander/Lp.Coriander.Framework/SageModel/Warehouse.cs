namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Warehouse")]
    public partial class Warehouse
    {
        public Warehouse()
        {
            Stocktakes = new HashSet<Stocktake>();
            WarehouseItems = new HashSet<WarehouseItem>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long WarehouseID { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Description { get; set; }

        public bool UseForSalesTrading { get; set; }

        [Required]
        [StringLength(60)]
        public string PostalName { get; set; }

        [Required]
        [StringLength(60)]
        public string AddressLine1 { get; set; }

        [Required]
        [StringLength(60)]
        public string AddressLine2 { get; set; }

        [Required]
        [StringLength(60)]
        public string AddressLine3 { get; set; }

        [Required]
        [StringLength(60)]
        public string AddressLine4 { get; set; }

        [Required]
        [StringLength(10)]
        public string PostCode { get; set; }

        [Required]
        [StringLength(50)]
        public string Contact { get; set; }

        [Required]
        [StringLength(30)]
        public string TelephoneNo { get; set; }

        [Required]
        [StringLength(30)]
        public string FaxNo { get; set; }

        [Required]
        [StringLength(255)]
        public string EmailAddress { get; set; }

        [Required]
        [StringLength(200)]
        public string WebAddress { get; set; }

        public DateTime? LastCompletedStocktake { get; set; }

        public long? CountryCodeID { get; set; }

        public long WarehouseTypeID { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] OpLock { get; set; }

        public DateTime DateTimeCreated { get; set; }

        [Required]
        [StringLength(100)]
        public string SpareText1 { get; set; }

        [Required]
        [StringLength(100)]
        public string SpareText2 { get; set; }

        [Required]
        [StringLength(100)]
        public string SpareText3 { get; set; }

        public decimal SpareNumber1 { get; set; }

        public decimal SpareNumber2 { get; set; }

        public decimal SpareNumber3 { get; set; }

        public DateTime? SpareDate1 { get; set; }

        public DateTime? SpareDate2 { get; set; }

        public DateTime? SpareDate3 { get; set; }

        public bool SpareBit1 { get; set; }

        public bool SpareBit2 { get; set; }

        public bool SpareBit3 { get; set; }

        [Required]
        [StringLength(60)]
        public string City { get; set; }

        [Required]
        [StringLength(60)]
        public string County { get; set; }

        [Required]
        [StringLength(60)]
        public string Country { get; set; }

        public virtual ICollection<Stocktake> Stocktakes { get; set; }

        public virtual WarehouseType WarehouseType { get; set; }

        public virtual ICollection<WarehouseItem> WarehouseItems { get; set; }
    }
}
