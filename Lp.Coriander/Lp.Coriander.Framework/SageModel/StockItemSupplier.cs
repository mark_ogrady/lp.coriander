namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StockItemSupplier")]
    public partial class StockItemSupplier
    {
        public long StockItemSupplierID { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ItemID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long SupplierID { get; set; }

        [Required]
        [StringLength(40)]
        public string SupplierStockCode { get; set; }

        public short LeadTime { get; set; }

        public long? LeadTimeUnitID { get; set; }

        public decimal UsualOrderQuantity { get; set; }

        public decimal MinimumOrderQuantity { get; set; }

        public bool Preferred { get; set; }

        public DateTime? DateLastOrder { get; set; }

        public decimal LastBuyingPrice { get; set; }

        public decimal LastOrderQuantity { get; set; }

        public decimal OrderQuantityYTD { get; set; }

        public decimal OrderValueYTD { get; set; }

        public decimal QuantityOnOrder { get; set; }

        public decimal LastBaseBuyingPrice { get; set; }

        public decimal CataloguePrice { get; set; }

        public DateTime? CataloguePriceDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] OpLock { get; set; }

        public DateTime DateTimeCreated { get; set; }

        public decimal ListPrice { get; set; }

        public decimal ListBasePrice { get; set; }

        public DateTime? DateListPriceChanged { get; set; }

        public DateTime? ListPriceExpiryDate { get; set; }

        public long DefaultPricingSourceTypeID { get; set; }

        public long LandedCostsTypeID { get; set; }

        public decimal LandedCostsValue { get; set; }

        public decimal ReorderMultipleQty { get; set; }

        public long? CountryOfOriginID { get; set; }

        public virtual StockItem StockItem { get; set; }
    }
}
