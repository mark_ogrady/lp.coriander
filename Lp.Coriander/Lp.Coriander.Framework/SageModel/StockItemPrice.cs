namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StockItemPrice")]
    public partial class StockItemPrice
    {
        public long StockItemPriceID { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ItemID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long PriceBandID { get; set; }

        public decimal Price { get; set; }

        public bool UseStandard { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] OpLock { get; set; }

        public DateTime DateTimeCreated { get; set; }

        public virtual StockItem StockItem { get; set; }
    }
}
