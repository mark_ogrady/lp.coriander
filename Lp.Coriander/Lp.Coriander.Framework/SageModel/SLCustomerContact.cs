namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SLCustomerContact")]
    public partial class SLCustomerContact
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long SLCustomerContactID { get; set; }

        public long SLCustomerAccountID { get; set; }

        [Required]
        [StringLength(235)]
        public string ContactName { get; set; }

        [Required]
        [StringLength(50)]
        public string Description { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] OpLock { get; set; }

        public DateTime DateTimeCreated { get; set; }

        public long SLCustomerLocationID { get; set; }

        public long SalutationID { get; set; }

        [Required]
        [StringLength(60)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(60)]
        public string MiddleName { get; set; }

        [Required]
        [StringLength(60)]
        public string LastName { get; set; }

        [Required]
        [StringLength(200)]
        public string ContactNamePreMigratedData { get; set; }

        public virtual SLCustomerAccount SLCustomerAccount { get; set; }
    }
}
