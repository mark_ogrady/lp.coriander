namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StocktakeLiveBinItem")]
    public partial class StocktakeLiveBinItem
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long StocktakeLiveBinItemID { get; set; }

        public long StocktakeID { get; set; }

        public long StocktakeItemID { get; set; }

        public long BinItemID { get; set; }

        public virtual Stocktake Stocktake { get; set; }

        public virtual StocktakeItem StocktakeItem { get; set; }
    }
}
