namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StocktakeItem")]
    public partial class StocktakeItem
    {
        public StocktakeItem()
        {
            StocktakeLiveBinItems = new HashSet<StocktakeLiveBinItem>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long StocktakeItemID { get; set; }

        public long StocktakeID { get; set; }

        public bool Selected { get; set; }

        public decimal AverageBuyingPrice { get; set; }

        public decimal HoldingValue { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] OpLock { get; set; }

        public DateTime DateTimeCreated { get; set; }

        [Required]
        [StringLength(30)]
        public string Details { get; set; }

        public virtual Stocktake Stocktake { get; set; }

        public virtual ICollection<StocktakeLiveBinItem> StocktakeLiveBinItems { get; set; }
    }
}
