namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WarehouseItem")]
    public partial class WarehouseItem
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long WarehouseItemID { get; set; }

        public long WarehouseID { get; set; }

        public long ItemID { get; set; }

        public decimal ReorderLevel { get; set; }

        public decimal MinimumLevel { get; set; }

        public decimal MaximumLevel { get; set; }

        public DateTime? DateOfLastSale { get; set; }

        public decimal ConfirmedQtyInStock { get; set; }

        public decimal UnconfirmedQtyInStock { get; set; }

        public decimal QuantityAllocatedSOP { get; set; }

        public decimal QuantityAllocatedStock { get; set; }

        public decimal QuantityOnPOPOrder { get; set; }

        public decimal HoldingValueAtBuyPrice { get; set; }

        public DateTime? DateOfLastStockCount { get; set; }

        public decimal PreReceiptAllocationQty { get; set; }

        public decimal QuantityAllocatedBOM { get; set; }

        public decimal QuantityReserved { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] OpLock { get; set; }

        public DateTime DateTimeCreated { get; set; }

        public virtual StockItem StockItem { get; set; }

        public virtual Warehouse Warehouse { get; set; }
    }
}
