namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Stocktake")]
    public partial class Stocktake
    {
        public Stocktake()
        {
            StocktakeCountSheetItems = new HashSet<StocktakeCountSheetItem>();
            StocktakeItems = new HashSet<StocktakeItem>();
            StocktakeLiveBinItems = new HashSet<StocktakeLiveBinItem>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long StocktakeID { get; set; }

        public long WarehouseID { get; set; }

        public long StocktakeSelectorID { get; set; }

        public long StockVarianceNomCodeID { get; set; }

        public DateTime? StocktakeDate { get; set; }

        public DateTime? CountingDate { get; set; }

        [Required]
        [StringLength(30)]
        public string NominalNarrative { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] OpLock { get; set; }

        public DateTime DateTimeCreated { get; set; }

        public bool ShowExpectedQuantity { get; set; }

        public long StocktakeStatusID { get; set; }

        public long StocktakeTypeID { get; set; }

        [Required]
        [StringLength(60)]
        public string StocktakeName { get; set; }

        public virtual StocktakeSelector StocktakeSelector { get; set; }

        public virtual StocktakeStatu StocktakeStatu { get; set; }

        public virtual StocktakeType StocktakeType { get; set; }

        public virtual Warehouse Warehouse { get; set; }

        public virtual ICollection<StocktakeCountSheetItem> StocktakeCountSheetItems { get; set; }

        public virtual ICollection<StocktakeItem> StocktakeItems { get; set; }

        public virtual ICollection<StocktakeLiveBinItem> StocktakeLiveBinItems { get; set; }
    }
}
