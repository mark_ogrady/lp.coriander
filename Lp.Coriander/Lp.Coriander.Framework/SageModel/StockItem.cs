namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StockItem")]
    public partial class StockItem
    {
        public StockItem()
        {
            AlternativeItems = new HashSet<AlternativeItem>();
            AlternativeItems1 = new HashSet<AlternativeItem>();
            StockItemNominalCodes = new HashSet<StockItemNominalCode>();
            StockItemPrices = new HashSet<StockItemPrice>();
            StockItemSuppliers = new HashSet<StockItemSupplier>();
            StockItemUnits = new HashSet<StockItemUnit>();
            StockItemUnitUOMTypes = new HashSet<StockItemUnitUOMType>();
            WarehouseItems = new HashSet<WarehouseItem>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ItemID { get; set; }

        [Required]
        [StringLength(30)]
        public string Code { get; set; }

        [Required]
        [StringLength(60)]
        public string Name { get; set; }

        public long ProductGroupID { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Description { get; set; }

        public bool UseDescriptionOnDocs { get; set; }

        public long TaxCodeID { get; set; }

        public decimal StandardCost { get; set; }

        public decimal SOPItemPrice { get; set; }

        public long? StockItemStatusID { get; set; }

        public DateTime? InactiveDate { get; set; }

        [Required]
        [StringLength(40)]
        public string Manufacturer { get; set; }

        [Required]
        [StringLength(40)]
        public string PartNumber { get; set; }

        public short StocktakeCyclePeriod { get; set; }

        [Required]
        [StringLength(8)]
        public string CommodityCode { get; set; }

        public decimal Weight { get; set; }

        public bool SuppressNetMass { get; set; }

        [Required]
        [StringLength(20)]
        public string StockUnitName { get; set; }

        [Required]
        [StringLength(20)]
        public string BaseUnitName { get; set; }

        public decimal StockMultOfBaseUnit { get; set; }

        [Required]
        [StringLength(30)]
        public string Barcode { get; set; }

        public DateTime? StdCostVarianceLastReset { get; set; }

        public decimal AverageBuyingPrice { get; set; }

        public long TraceableTypeID { get; set; }

        public bool SaleFromSingleBatch { get; set; }

        public bool AllowDuplicateNumbers { get; set; }

        public bool UsesAlternativeRef { get; set; }

        public bool UsesSellByDate { get; set; }

        public bool UsesUseByDate { get; set; }

        public bool RecordNosOnGoodsReceived { get; set; }

        public DateTime? LastArchivedUpTo { get; set; }

        public decimal FreeStockQuantity { get; set; }

        public long? BOMItemTypeID { get; set; }

        public long SOPOrderFulfilmentMethodID { get; set; }

        [Required]
        [StringLength(160)]
        public string DefaultDespatchNoteComment { get; set; }

        [Required]
        [StringLength(160)]
        public string DefaultPickingListComment { get; set; }

        public decimal QuantityReserved { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] OpLock { get; set; }

        public DateTime DateTimeCreated { get; set; }

        public DateTime? LastTraceArchivedUpTo { get; set; }

        public long LandedCostsTypeID { get; set; }

        public decimal LandedCostsValue { get; set; }

        [Required]
        [StringLength(100)]
        public string SpareText1 { get; set; }

        [Required]
        [StringLength(100)]
        public string SpareText2 { get; set; }

        [Required]
        [StringLength(100)]
        public string SpareText3 { get; set; }

        public decimal SpareNumber1 { get; set; }

        public decimal SpareNumber2 { get; set; }

        public decimal SpareNumber3 { get; set; }

        public DateTime? SpareDate1 { get; set; }

        public DateTime? SpareDate2 { get; set; }

        public DateTime? SpareDate3 { get; set; }

        public bool SpareBit1 { get; set; }

        public bool SpareBit2 { get; set; }

        public bool SpareBit3 { get; set; }

        public bool AllowSalesOrder { get; set; }

        public long STKAutoGenerateOptionTypeID { get; set; }

        [Required]
        [StringLength(20)]
        public string AutoGeneratePrefix { get; set; }

        public long AutoGenerateNextNumber { get; set; }

        public long STKLabelPrintingOptionTypeID { get; set; }

        public long STKFulfilmentSequenceTypeID { get; set; }

        public int ShelfLife { get; set; }

        public long? STKShelfLifeTypeID { get; set; }

        public bool AllowOutOfDate { get; set; }

        public long STKAutoGenerateSeparatorID { get; set; }

        public int AutoGeneratePadding { get; set; }

        public long? CountryOfOriginID { get; set; }

        public bool UsesSupplementaryUnit { get; set; }

        public decimal SupplementaryUnitConversionRatio { get; set; }

        public bool UsesRCV { get; set; }

        public bool IsWEEEItem { get; set; }

        public bool IncludeNosOnCountSheets { get; set; }

        public virtual ICollection<AlternativeItem> AlternativeItems { get; set; }

        public virtual ICollection<AlternativeItem> AlternativeItems1 { get; set; }

        public virtual StockItemStatu StockItemStatu { get; set; }

        public virtual ICollection<StockItemNominalCode> StockItemNominalCodes { get; set; }

        public virtual ICollection<StockItemPrice> StockItemPrices { get; set; }

        public virtual ICollection<StockItemSupplier> StockItemSuppliers { get; set; }

        public virtual ICollection<StockItemUnit> StockItemUnits { get; set; }

        public virtual ICollection<StockItemUnitUOMType> StockItemUnitUOMTypes { get; set; }

        public virtual ICollection<WarehouseItem> WarehouseItems { get; set; }
    }
}
