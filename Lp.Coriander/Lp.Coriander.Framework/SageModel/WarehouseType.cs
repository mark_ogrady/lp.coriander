namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WarehouseType")]
    public partial class WarehouseType
    {
        public WarehouseType()
        {
            Warehouses = new HashSet<Warehouse>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long WarehouseTypeID { get; set; }

        [Required]
        [StringLength(20)]
        public string WarehouseTypeName { get; set; }

        public virtual ICollection<Warehouse> Warehouses { get; set; }
    }
}
