namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class StocktakeStatu
    {
        public StocktakeStatu()
        {
            Stocktakes = new HashSet<Stocktake>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long StocktakeStatusID { get; set; }

        [Required]
        [StringLength(20)]
        public string StocktakeStatusName { get; set; }

        public virtual ICollection<Stocktake> Stocktakes { get; set; }
    }
}
