namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StockItemUnitUOMType")]
    public partial class StockItemUnitUOMType
    {
        public long StockItemUnitUOMTypeID { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long StockItemUnitID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long UnitOfMeasureTypeID { get; set; }

        public long StockItemID { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] OpLock { get; set; }

        public DateTime DateTimeCreated { get; set; }

        public virtual StockItem StockItem { get; set; }

        public virtual StockItemUnit StockItemUnit { get; set; }
    }
}
