namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StocktakeType")]
    public partial class StocktakeType
    {
        public StocktakeType()
        {
            Stocktakes = new HashSet<Stocktake>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long StocktakeTypeId { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        public virtual ICollection<Stocktake> Stocktakes { get; set; }
    }
}
