namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StockSetting")]
    public partial class StockSetting
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long StockSettingID { get; set; }

        public bool UseMultipleLocations { get; set; }

        public bool UseSearchCategories { get; set; }

        public bool IntegrateWithNominal { get; set; }

        public bool ProfitPercentOfRevenue { get; set; }

        public bool UpdateLabelFile { get; set; }

        public bool AllocateIndividualItems { get; set; }

        public bool RecordNosOnGoodsReceived { get; set; }

        public DateTime? AllItemsLastArchivedUpTo { get; set; }

        [StringLength(30)]
        public string LastArchiveRunBy { get; set; }

        public DateTime? LastArchiveRunOn { get; set; }

        [StringLength(30)]
        public string LastPurgeArchiveRunBy { get; set; }

        public DateTime? LastPurgeArchiveRunOn { get; set; }

        public DateTime? ArchiveLastPurgedUpTo { get; set; }

        [StringLength(30)]
        public string LastProveBalancesRunBy { get; set; }

        public DateTime? LastProveBalancesRunOn { get; set; }

        public bool AllowFulfilmentFromStock { get; set; }

        public bool OKFulfilmntFromSuppViaWarhouse { get; set; }

        public bool OKFulfilmntDirectToCustomer { get; set; }

        public bool UseCurrentStockForFulfilment { get; set; }

        public bool PostCOSForSOP { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] OpLock { get; set; }

        public DateTime DateTimeCreated { get; set; }

        public bool AllowNegStockTake { get; set; }

        public bool AllowNegStockTakeWithPO { get; set; }

        [Required]
        [StringLength(30)]
        public string LastTraceArchiveRunBy { get; set; }

        public DateTime? LastTraceArchiveRunOn { get; set; }

        [Required]
        [StringLength(30)]
        public string LastTracePurgeArchiveRunBy { get; set; }

        public DateTime? LastTracePurgeArchiveRunOn { get; set; }

        public DateTime? AllTraceItemsLastArchivedUpTo { get; set; }

        public DateTime? TraceArchiveLastPurgedUpTo { get; set; }

        public bool UseLandedCosts { get; set; }

        public long? LandedCostsNominalCode { get; set; }
    }
}
