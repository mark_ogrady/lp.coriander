namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StockItemType")]
    public partial class StockItemType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long StockItemTypeID { get; set; }

        [Required]
        [StringLength(20)]
        public string StockItemTypeName { get; set; }
    }
}
