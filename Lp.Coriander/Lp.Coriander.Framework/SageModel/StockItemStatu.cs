namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class StockItemStatu
    {
        public StockItemStatu()
        {
            StockItems = new HashSet<StockItem>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long StockItemStatusID { get; set; }

        [Required]
        [StringLength(20)]
        public string StockItemStatusName { get; set; }

        public virtual ICollection<StockItem> StockItems { get; set; }
    }
}
