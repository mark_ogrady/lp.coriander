namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StocktakeSelector")]
    public partial class StocktakeSelector
    {
        public StocktakeSelector()
        {
            Stocktakes = new HashSet<Stocktake>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long StocktakeSelectorID { get; set; }

        [Required]
        [StringLength(20)]
        public string StocktakeSelectorName { get; set; }

        public virtual ICollection<Stocktake> Stocktakes { get; set; }
    }
}
