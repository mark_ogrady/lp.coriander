namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SLCustomerAccount")]
    public partial class SLCustomerAccount
    {
        public SLCustomerAccount()
        {
            SLCustomerAccount1 = new HashSet<SLCustomerAccount>();
            SLCustomerContacts = new HashSet<SLCustomerContact>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long SLCustomerAccountID { get; set; }

        [Required]
        [StringLength(8)]
        public string CustomerAccountNumber { get; set; }

        [Required]
        [StringLength(60)]
        public string CustomerAccountName { get; set; }

        [Required]
        [StringLength(8)]
        public string CustomerAccountShortName { get; set; }

        public decimal AccountBalance { get; set; }

        public decimal CreditLimit { get; set; }

        public long SYSCurrencyID { get; set; }

        public long SYSExchangeRateTypeID { get; set; }

        public long SYSCountryCodeID { get; set; }

        public long DefaultSYSTaxRateID { get; set; }

        [Required]
        [StringLength(30)]
        public string TaxRegistrationNumber { get; set; }

        public short MonthsToKeepTransactionsFor { get; set; }

        [Required]
        [StringLength(1)]
        public string DefaultOrderPriority { get; set; }

        public long? SLFinanceChargeID { get; set; }

        [Required]
        [StringLength(8)]
        public string DefaultNominalAccountNumber { get; set; }

        [Required]
        [StringLength(3)]
        public string DefaultNominalCostCentre { get; set; }

        [Required]
        [StringLength(3)]
        public string DefaultNominalDepartment { get; set; }

        public long SYSAccountTypeID { get; set; }

        public decimal EarlySettlementDiscountPercent { get; set; }

        public short DaysEarlySettlementDiscApplies { get; set; }

        public short PaymentTermsInDays { get; set; }

        public long SYSPaymentTermsBasisID { get; set; }

        public bool UseConsolidatedBilling { get; set; }

        public decimal InvoiceLineDiscountPercent { get; set; }

        public decimal InvoiceDiscountPercent { get; set; }

        public long SLAssociatedOfficeTypeID { get; set; }

        public long? AssociatedHeadOfficeAccountID { get; set; }

        public bool SendCopyStatementToBranch { get; set; }

        public long? CustomerDiscountGroupID { get; set; }

        public long? PriceBandID { get; set; }

        public long? OrderValueDiscountID { get; set; }

        public bool AccountIsOnHold { get; set; }

        public decimal ValueOfCurrentOrdersInSOP { get; set; }

        public DateTime DateAccountDetailsLastChanged { get; set; }

        public DateTime? DateOfLastTransaction { get; set; }

        [StringLength(8)]
        public string EuroAccountNumberCopiedFromTo { get; set; }

        public DateTime? DateEuroAccountCopied { get; set; }

        public bool UseTransactionEMail { get; set; }

        public DateTime? DateFinanceChargeLastRun { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] OpLock { get; set; }

        public DateTime DateTimeCreated { get; set; }

        public long? SYSCreditBureauID { get; set; }

        public long? SYSCreditPositionID { get; set; }

        [Required]
        [StringLength(30)]
        public string TradingTerms { get; set; }

        [Required]
        [StringLength(60)]
        public string CreditReference { get; set; }

        public int AverageTimeToPay { get; set; }

        public DateTime? AccountOpened { get; set; }

        public DateTime? LastCreditReview { get; set; }

        public DateTime? NextCreditReview { get; set; }

        public DateTime? ApplicationDate { get; set; }

        public DateTime? DateReceived { get; set; }

        public bool TermsAgreed { get; set; }

        public bool UseTaxCodeAsDefault { get; set; }

        public DateTime? AvgTimeToPayDateLastUpdated { get; set; }

        [Required]
        [StringLength(100)]
        public string SpareText1 { get; set; }

        [Required]
        [StringLength(100)]
        public string SpareText2 { get; set; }

        [Required]
        [StringLength(100)]
        public string SpareText3 { get; set; }

        public decimal SpareNumber1 { get; set; }

        public decimal SpareNumber2 { get; set; }

        public decimal SpareNumber3 { get; set; }

        public DateTime? SpareDate1 { get; set; }

        public DateTime? SpareDate2 { get; set; }

        public DateTime? SpareDate3 { get; set; }

        public bool SpareBit1 { get; set; }

        public bool SpareBit2 { get; set; }

        public bool SpareBit3 { get; set; }

        [Required]
        [StringLength(9)]
        public string DUNSCode { get; set; }

        [Required]
        [StringLength(20)]
        public string MainTelephoneAreaCode { get; set; }

        [Required]
        [StringLength(5)]
        public string MainTelephoneCountryCode { get; set; }

        [Required]
        [StringLength(200)]
        public string MainTelephoneSubscriberNumber { get; set; }

        [Required]
        [StringLength(20)]
        public string MainFaxAreaCode { get; set; }

        [Required]
        [StringLength(5)]
        public string MainFaxCountryCode { get; set; }

        [Required]
        [StringLength(200)]
        public string MainFaxSubscriberNumber { get; set; }

        [Required]
        [StringLength(200)]
        public string MainWebsite { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode1 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode2 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode3 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode4 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode5 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode6 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode7 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode8 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode9 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode10 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode11 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode12 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode13 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode14 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode15 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode16 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode17 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode18 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode19 { get; set; }

        [Required]
        [StringLength(60)]
        public string AnalysisCode20 { get; set; }

        public virtual ICollection<SLCustomerAccount> SLCustomerAccount1 { get; set; }

        public virtual SLCustomerAccount SLCustomerAccount2 { get; set; }

        public virtual ICollection<SLCustomerContact> SLCustomerContacts { get; set; }
    }
}
