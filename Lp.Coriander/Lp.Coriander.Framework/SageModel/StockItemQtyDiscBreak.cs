namespace Lp.Coriander.Framework.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StockItemQtyDiscBreak")]
    public partial class StockItemQtyDiscBreak
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long StockItemQtyDiscBreakID { get; set; }

        public decimal QuantityMoreThan { get; set; }

        public decimal DiscountAmountValue { get; set; }

        public decimal DiscountPercentValue { get; set; }

        public long StockItemDiscountID { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] OpLock { get; set; }

        public DateTime DateTimeCreated { get; set; }
    }
}
