﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lp.Coriander.Framework.Data;
using Lp.Coriander.Framework.Model;

namespace Lp.Coriander.Framework.Seed
{
    public static class SeedMenu
    {
        public static void CreateStandardMenu(LpContext context)
        {
            var menuList = new List<Menu>()
            {
                new Menu()
                {
                    MenuId = Guid.NewGuid(),
                    MenuName = "Sales Order",
                    MenuType = 2,
                    Link = "/SOP",
                    Icon = "fa-pencil-square-o",
                    OrderPosition = 1,
                    MenuItems = new Collection<MenuItem>()
                    {
                        new MenuItem()
                        {
                            MenuItemId = Guid.NewGuid(),
                            Icon = "",
                            ItemName = "Create",
                            Link = "/SOP/Create",
                            OrderPosition = 1,
                        }
                    }
                },

                 new Menu()
                {
                    MenuId = Guid.NewGuid(),
                    MenuName = "Purchase Order",
                    MenuType = 2,
                    Link = "/Pop",
                    Icon = "fa-pencil-square-o",
                    OrderPosition = 2
                },

                   new Menu()
                {
                    MenuId = Guid.NewGuid(),
                    MenuName = "Stock",
                    MenuType = 2,
                    Link = "/Stock",
                    Icon = "fa-pencil-square-o",
                    OrderPosition = 3
                },

                  new Menu()
                {
                    MenuId = Guid.NewGuid(),
                    MenuName = "Nominal Ledger",
                    MenuType = 2,
                    Link = "/nominalLedger",
                    Icon = "fa-pencil-square-o",
                    OrderPosition = 4
                },

                    new Menu()
                {
                    MenuId = Guid.NewGuid(),
                    MenuName = "Purchase Ledger",
                    MenuType = 2,
                    Link = "/nominalLedger",
                    Icon = "fa-pencil-square-o",
                    OrderPosition = 5
                },

                    new Menu()
                {
                    MenuId = Guid.NewGuid(),
                    MenuName = "Sales Ledger",
                    MenuType = 2,
                    Link = "/nominalLedger",
                    Icon = "fa-pencil-square-o",
                    OrderPosition = 6
                },
                  new Menu()
                {
                    MenuId = Guid.NewGuid(),
                    MenuName = "Settings",
                    MenuType = 2,
                    Link = "/settings",
                    Icon = "fa-pencil-square-o",
                    OrderPosition = 7
                },
            };

            foreach (var menu in menuList)
            {
                var existingMenu = context.Menus.FirstOrDefault(p => p.MenuName == menu.MenuName && p.MenuType == menu.MenuType);
                if (existingMenu == null)
                {
                    context.Menus.Add(menu);
                    foreach(var  menuItem in menu.MenuItems)
                    {
                        var existingMenuItem = context.MenuItems.FirstOrDefault(mi => mi.ItemName == menuItem.ItemName && mi.QcMenu.MenuName == menu.MenuName);
                        if (existingMenuItem != null)
                        {
                            context.MenuItems.Add(menuItem);
                        }


                    }

                }

            }
            context.SaveChanges();
        }
    }
}
