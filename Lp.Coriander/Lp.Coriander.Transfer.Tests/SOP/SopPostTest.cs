﻿using System.Configuration;
using System.Diagnostics;
using System.Runtime.Remoting.Messaging;
using System.Web.Helpers;
using Lp.Coriander.Framework.Http;
using Lp.Coriander.TransferModels.SOP;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Serialization;

namespace Lp.Coriander.Transfer.Tests.SOP
{
    [TestClass]
    public class SopPostTest
    {
        [TestMethod]
        public void PostSOPOrderTest()
        {
            bool complete = false;
            var url = ConfigurationManager.AppSettings["SiteUrl"];
            var username = ConfigurationManager.AppSettings["UserName"];
            var password = ConfigurationManager.AppSettings["Password"];


            var sopOrder = new SopOrder();
            var retu = 0;
            var data = Newtonsoft.Json.JsonConvert.SerializeObject(sopOrder);
            HttpTransfer httpTransfer = new HttpTransfer();

            httpTransfer.Post<SopOrder>(
                url,
                data,
                username,
                password,
                success =>
                {
                    sopOrder = success;
                    complete = true;
                },
                error =>
                {
                    Debug.WriteLine(error.Message);
                });

            Assert.IsTrue(complete);
        }
    }

 
}
